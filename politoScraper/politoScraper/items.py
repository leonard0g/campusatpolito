# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field


class PolitoscraperItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
	location = Field()
	next_url = Field()
	site = Field()
	floor = Field()
	details = Field()
	building = Field()
	lat = Field()
	lng = Field()
	organization = Field()
	type = Field()

class Collegio(scrapy.Item):
	name = Field()	#nome collegio
	#cod = Field() #codice collegio, dall'url
	department = Field() #nome dipartimento
	dep_code = Field()	#codice dipartimento, es. DAUIN
	dep_link = Field() #sito dipartimento
	#coord = Field()	#matricola
	next_url = Field()	#link alla pagina con i docenti (collegio)

class Teacher(scrapy.Item):
	#next_url = Field()
	id_number = Field()
	name = Field()
	surname = Field()
	role = Field()
	#picture = Field()
	department_id = Field()
	email = Field()
	tel = Field()
	#www = Field()
	course_ids = Field()

class Exam(scrapy.Item):
	course_id = Field()
	site = Field()
	location = Field()
	exam_type = Field()
	date = Field()
	time = Field()
	end_booking_date = Field()
	end_booking_time = Field()

	



