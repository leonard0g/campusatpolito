import scrapy
import re
from scrapy.selector import Selector
from scrapy import Request
from politoScraper.items import *
from selenium import webdriver
from selenium.webdriver.common.by import By
import csv


class PolitoscraperSpider(scrapy.Spider):
	name = 'teachers2'
	allowed_domains = ["polito.it"]
	start_urls = list()
	items = dict()

	def __init__(self, input_file):

		dictReader = csv.DictReader(open(input_file, 'rb'), fieldnames = ['name', 'surname', 'url'], delimiter = ',')

		all_rows = 0
		filtered_rows = 0
		for row in dictReader:
			all_rows += 1
			url = row['url'].strip()
			if url not in self.start_urls:
				filtered_rows += 1
				self.start_urls.append(row['url'].strip())
			if url not in self.items:
				t = Teacher()
				t['name'] = row['name']
				t['surname'] = row['surname']
				self.items[url] = t
				#print row['name'], row['surname'], url
		
		print "total #links (input): ", all_rows
		print "final #links (duplicates filtered out): ", filtered_rows, "(", len(self.items.keys()), " dict keys)"			
			


	def parse(self, response):
		sel = Selector(response)

		url = response.url
		t = self.items[url]

		m = re.match(".*m=(\d+)", url)
		t['id_number'] = 'd' + m.group(1)	

		tab1 = sel.xpath('//*[@id="tabs-1"]')
		tab1 = tab1[0] #sempre 1 solo

		email = tab1.xpath('.//p/a/text()').extract()
		email = ''.join(email).strip()
		m = re.search(r'[a-z.]+@[a-z.]+\.it', email)
		if m:		
			t['email'] = m.group(0)

		p_tags = tab1.xpath('.//p/text()').extract()
		p_tags = ''.join(p_tags).strip()
		#print "p = ", p_tags #####

		match = False
		for match in re.finditer(r'.*Tel:[ \t]+([0-9]+).*', p_tags):
			pass
		if match:
			t['tel'] = match.group(1)

		t['role'] = ""
		m = re.findall(r'^([A-Za-z][a-z.]+ [A-Za-z][a-z]+( [A-Za-z][a-z]+)?)', p_tags)
		if m:
			for match in m:	
				t['role'] += match[0]

		m = re.search(r'([A-Z]{3,})[ \t]+-', p_tags)
		if m:		
			t['department_id'] = m.group(1)

		tab2 = sel.xpath('//*[@id="tabs-2"]')
		tab2 = tab2[0]
		elems = tab2.xpath('.//td')
		anno_acc = 0
		courses = list()
		for e in elems:			
			if anno_acc == 2:
				break
			text = e.xpath('.//span/text()').extract()
			text = ''.join(text)
			if 'Materiale' in text:
				anno_acc += 1
			else:
				c = e.xpath('text()').extract()
				c = ''.join(c)
				courses.append(c)
			
		courses = ''.join(courses).strip().strip('\n')
		t['course_ids'] = ""
		for match in re.finditer(r'[0-9]+[A-Z]+', courses):
			if match:
				#print match.group()
				if t['course_ids'] != "":
					t['course_ids'] += " "
				t['course_ids'] += match.group()
				
		return t	

