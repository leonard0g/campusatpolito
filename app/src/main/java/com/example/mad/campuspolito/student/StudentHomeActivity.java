package com.example.mad.campuspolito.student;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.HomeActivity;
import com.example.mad.campuspolito.common.LectureDetailActivity;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.common.ShowCourseDetailsActivity;
import com.example.mad.campuspolito.common.TimetableListActivity;
import com.example.mad.campuspolito.locations.LocationSearchMasterActivity;
import com.example.mad.campuspolito.model.AppUser;

import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.offer.OfferListActivity;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class StudentHomeActivity extends HomeActivity {

    private TextView mHomeName;
    private TextView mHomeDescription;
    private TextView mStudentDegreeTitle;
    private TextView mStudentDegreeLevel;
    private CircularImageView mHomeImage;
    private LinearLayout mLatestMessages;
    private LinearLayout mProgress;
    private NestedScrollView mContent;
    private LinearLayout mTodayLessons;
    private TextView mToday;

    private Student profile;
    private FetchHomeData task;

    @Override
    protected void onSetHomeContentView() throws ParseException{
        setContentView(R.layout.activity_student_home);

        mHomeName = (TextView)findViewById(R.id.home_name);
        mHomeDescription = (TextView)findViewById(R.id.home_description);
        mStudentDegreeTitle = (TextView)findViewById(R.id.student_degree_title);
        mStudentDegreeLevel = (TextView)findViewById(R.id.student_degree_level);
        mHomeImage = (CircularImageView)findViewById(R.id.home_image);
        mLatestMessages = (LinearLayout)findViewById(R.id.latest_messages);
        mProgress = (LinearLayout)findViewById(R.id.home_progress);
        mContent = (NestedScrollView)findViewById(R.id.home_content);
        mTodayLessons = (LinearLayout)findViewById(R.id.today_lessons);
        mToday = (TextView)findViewById(R.id.today);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(false);
    }

    public class FetchHomeData extends AsyncTask<Void,Void,Boolean> {

        private final Context ctx;
        List<Conversation> latestConversations;
        List<Lecture> todayLectures;

        public FetchHomeData(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                profile = currentUser.getStudentProfile();
                profile.fetchIfNeeded();
                profile.getDegree().fetchIfNeeded();


                Log.d(TAG, "Basic info about student loaded.");

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Basic info about student has been loaded.");
                    return false;
                }


                if(profile.getCourses() != null){

                    ParseQuery<Lecture> lessonsQuery = ParseQuery.getQuery(Lecture.class);

                    Calendar c = Calendar.getInstance();
                    lessonsQuery.whereEqualTo(Lecture.DAY, c.get(Calendar.DAY_OF_MONTH));
                    lessonsQuery.whereEqualTo(Lecture.MONTH, c.get(Calendar.MONTH));
                    lessonsQuery.whereEqualTo(Lecture.YEAR, c.get(Calendar.YEAR));
                    lessonsQuery.whereContainedIn(Lecture.COURSE, profile.getCourses());
                    lessonsQuery.orderByAscending(Lecture.START_SLOT);
                    lessonsQuery.include(Lecture.COURSE);
                    lessonsQuery.include(Lecture.ROOM);

                    todayLectures = new ArrayList<Lecture>(lessonsQuery.find());

                }
                else
                    todayLectures = new ArrayList<>();

                Log.d(TAG, "Info about lectures loaded.");

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Courses info about student has been loaded.");
                    return false;
                }

                String sessionToken = AppUser.getCurrentUser().getSessionToken();
                ParseQuery<Conversation> conversationQuery = ParseQuery.getQuery(Conversation.class);
                conversationQuery.whereContainedIn(Conversation.USERS, Arrays.asList(currentUser));
                conversationQuery.orderByDescending("updatedAt");
                conversationQuery.setLimit(3);
                conversationQuery.include(Conversation.USERS);
                conversationQuery.whereExists(Conversation.LAST_MSG);


                latestConversations = new ArrayList<Conversation>(conversationQuery.find());

                AppUser.become(sessionToken);

                Log.d(TAG, "Info about conversations loaded.");

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Basic info about conversations removed.");
                    return false;
                }

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching home data.",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){

                mToday.setText(Calendar.getInstance().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) +
                    " " + android.text.format.DateFormat.getDateFormat(ctx).format(Calendar.getInstance().getTime()));
                mHomeName.setText(currentUser.toString());
                mHomeDescription.setText(profile.getIdNumber());
                mStudentDegreeTitle.setText(profile.getDegree().getTitle());
                mStudentDegreeLevel.setText(profile.getDegree().getLevel());
                if(currentUser.getPicture() != null){
                    mHomeImage.setParseFile(currentUser.getPicture());
                    mHomeImage.loadInBackground();
                }
                else {
                    mHomeImage.setParseFile(null);
                    mHomeImage.setPlaceholder(getResources().getDrawable(R.drawable.polito));
                }

                if(todayLectures.size() == 0){
                    findViewById(R.id.no_today_lessons).setVisibility(View.VISIBLE);
                    mTodayLessons.removeAllViews();
                }
                else {
                    findViewById(R.id.no_today_lessons).setVisibility(View.GONE);
                    DynamicLinearLayoutViewInflater.addLectureViewsToLayout(todayLectures,
                            ctx, mTodayLessons, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String lectureId = (String) v.getTag(R.id.today_lessons);
                                    if (lectureId != null) {
                                        Intent showLectureDetails = new Intent(ctx, LectureDetailActivity.class);
                                        showLectureDetails.putExtra(LectureDetailActivity.LECTURE_ID, lectureId);
                                        startActivity(showLectureDetails);
                                    }

                                }
                            }, R.id.today_lessons);
                }

                if(latestConversations.size() == 0){
                    findViewById(R.id.no_latest_messages).setVisibility(View.VISIBLE);
                    mLatestMessages.removeAllViews();
                }
                else {
                    findViewById(R.id.no_latest_messages).setVisibility(View.GONE);
                    DynamicLinearLayoutViewInflater.addConversationViewsToLayout(latestConversations,
                            ctx, mLatestMessages, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent showConversation = new Intent(ctx, ConversationListActivity.class);
                                    showConversation.putExtra(ConversationListActivity.INTENT_DISPLAY_CONVERSATION, (String) v.getTag(R.id.latest_messages));
                                    startActivity(showConversation);
                                }
                            }, R.id.latest_messages);
                }
            }
            else {
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
                Snackbar.make(mContent,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();
            }

            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
        }

        @Override
        protected void onCancelled() {
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            Snackbar.make(mContent,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onSubscribeToPushChannels() {

        try {
            if(profile == null)
                profile = currentUser.getStudentProfile();

            profile.fetchIfNeeded();

            List<Course> courseList = profile.getCourses();

            if(courseList != null && courseList.size()> 0){

                ParseObject.fetchAllIfNeeded(courseList);

                Log.d(TAG, "Current installation id:" + ParseInstallation.getCurrentInstallation().getInstallationId());
                Log.d(TAG, "Current channels are:" + ParseInstallation.getCurrentInstallation().getList("channels"));

                for(Course course : courseList){
                    if(!ParseInstallation.getCurrentInstallation().getList("channels").contains(CampusPolitoApplication.COURSE_CHANNEL+course.getObjectId())){
                        Log.d(TAG, "Subscribing to the channel " + CampusPolitoApplication.COURSE_CHANNEL + course.getObjectId());

                        ParsePush.subscribeInBackground(CampusPolitoApplication.COURSE_CHANNEL + course.getObjectId(), new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e != null) {
                                    Log.e(TAG, "Error while subscribing in background to course channel.", e);
                                } else
                                    Log.d(TAG, "Course channel subscribed.");
                            }
                        });
                    }
                }
            }

            ParseQuery<Conversation> query = ParseQuery.getQuery(Conversation.class);
            query.whereContainedIn(Conversation.USERS, Arrays.asList(currentUser));

            query.findInBackground(new FindCallback<Conversation>() {
                @Override
                public void done(List<Conversation> list, ParseException e) {
                    if (e != null) {
                        Log.e(TAG, "Error while fetching student conversations", e);
                    } else {
                        for (Conversation c : list) {
                            if (!ParseInstallation.getCurrentInstallation().getList("channels").contains(CampusPolitoApplication.CONVERSATION_CHANNEL + c.getObjectId())) {
                                Log.d(TAG, "Subscribing to the channel " + CampusPolitoApplication.CONVERSATION_CHANNEL + c.getObjectId());

                                ParsePush.subscribeInBackground(CampusPolitoApplication.CONVERSATION_CHANNEL + c.getObjectId(), new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e != null) {
                                            Log.e(TAG, "Error while subscribing in background to conversation channel.", e);
                                        } else
                                            Log.d(TAG, "Conversation channel subscribed.");
                                    }
                                });
                            }
                        }
                    }
                }
            });


        } catch (ParseException e) {
            Log.d(TAG, "Error with Parse query",e);
        }

        if(!ParseInstallation.getCurrentInstallation().getList("channels").contains(CampusPolitoApplication.STUDENT_CHANNEL+currentUser.getObjectId())){
            Log.d(TAG, "Creating student channel for notifications.");
            ParsePush.subscribeInBackground(CampusPolitoApplication.STUDENT_CHANNEL + currentUser.getObjectId(), new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        Log.e(TAG, "Error while subscribing in background to student channel.", e);
                    } else
                        Log.d(TAG, "Student channel subscribed.");
                }
            });
        }

    }

    @Override
    protected boolean OnDrawerMenuItemSelected(int itemId) {
        switch (itemId) {

            case R.id.student_career_plan:
                Intent careerPlanIntent = new Intent(this,CareerPlanActivity.class);
                startActivity(careerPlanIntent);
                return true;

            case R.id.student_noticeboard:
                Intent noticesIntent = new Intent(this,NoticesListActivity.class);
                startActivity(noticesIntent);
                return true;

            case R.id.student_search_locations:
                Intent locationsIntent = new Intent(this, LocationSearchMasterActivity.class);
                startActivity(locationsIntent);
                return true;
            case R.id.student_timetable:
                Intent intent = new Intent(this,TimetableListActivity.class);
                startActivity(intent);
                return true;
            case R.id.student_temporary_jobs:
                Intent jobsIntent = new Intent(this, OfferListActivity.class);
                startActivity(jobsIntent);
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void onInflateHomeMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_student, menu);
    }

    @Override
    protected boolean onHomeMenuItemSelected(int id) {

        //noinspection SimplifiableIfStatement


        if(id == R.id.action_conversations) {
            Intent conversationsIntent = new Intent(this, ConversationListActivity.class);
            startActivity(conversationsIntent);
            return true;
        }
        else return false;
    }

    @Override
    protected void onHomeViewClick(View v) {

    }

    @Override
    protected void onResumeHomeActivity() {
        task = new FetchHomeData(this);
        task.execute();
    }
}
