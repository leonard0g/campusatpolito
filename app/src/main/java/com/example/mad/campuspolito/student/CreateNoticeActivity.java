package com.example.mad.campuspolito.student;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.FullscreenImageActivity;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.example.mad.campuspolito.utils.ParseApplicationGlobalsQuery;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;
import com.wefika.flowlayout.FlowLayout;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import bolts.Task;
import fr.ganfra.materialspinner.MaterialSpinner;

public class CreateNoticeActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final String TAG = CreateNoticeActivity.class.getName();
    private FloatingActionButton mCreateNotice;
    private static final String TAGS_LIST = "TAGS_LIST";
    private static final String PICTURES_LIST = "PICTURES_LIST";
    private static final String NEW_PICTURE_LIST = "NEW_PICTURES_LIST";
    private static final String TITLE = "TITLE";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String COST = "COST";
    private static final String CATEGORY = "CATEGORY";
    private static final String NOTICE_CATEGORIES = "NOTICE_CATEGORIES";

    private static final int RESULT_LOAD_IMG = 0;

    private NoticeCreationTask mNoticeTask = null;

    private FlowLayout searchTagsFlowLayout;
    private FloatingActionButton addSearchTagButton;
    private AutoCompleteTextView tagView;
    private String tagString;
    private ArrayList<String> tagList;
    private TextView noTagsText;
    private LinearLayout photosStaticGridView;
    private Button uploadButton;
    private ArrayList<String> pictureList;

    private Toolbar mToolbar;

    private ArrayList<Uri> newPictureList;

    private EditText mTitleField;
    private EditText mDescriptionField;
    private EditText mCostField;
    private MaterialSpinner mCategory;
    private ArrayList<String> noticeCategories;


    private String title;
    private String description;
    private String cost;
    private String category;

    private NestedScrollView mFormView;
    private LinearLayout mProgressView;

    //This parameter is set only if modifying the notice
    private String noticeId = null;
    public static final String NOTICE_ID = "NOTICE_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notice);

        mFormView = (NestedScrollView) findViewById(R.id.notice_form);
        mProgressView = (LinearLayout) findViewById(R.id.notice_progress);
        mCreateNotice = (FloatingActionButton)findViewById(R.id.create_notice_button);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if(getIntent() != null && getIntent().hasExtra(NOTICE_ID))
            noticeId = getIntent().getStringExtra(NOTICE_ID);
        else
            noticeId = null;

        if(noticeId != null)
            getSupportActionBar().setTitle(R.string.title_activity_modify_notice);


        if (savedInstanceState != null) {

            title = savedInstanceState.getString(TITLE);
            description = savedInstanceState.getString(DESCRIPTION);
            cost = savedInstanceState.getString(COST);
            noticeId = savedInstanceState.getString(NOTICE_ID);
            category = savedInstanceState.getString(CATEGORY);
        }

        if(savedInstanceState == null|| savedInstanceState.getStringArrayList(TAGS_LIST) == null)
            tagList = new ArrayList<>();
        else
            tagList = savedInstanceState.getStringArrayList(TAGS_LIST);

        if(savedInstanceState == null || savedInstanceState.getStringArrayList(PICTURES_LIST) == null)
            pictureList = new ArrayList<>();
        else
            pictureList = savedInstanceState.getStringArrayList(PICTURES_LIST);

        if(savedInstanceState == null || savedInstanceState.getParcelableArrayList(NEW_PICTURE_LIST) == null)
            newPictureList = new ArrayList<>();
        else
            newPictureList = savedInstanceState.getParcelableArrayList(NEW_PICTURE_LIST);


        mCategory = (MaterialSpinner)findViewById(R.id.category);
        if(savedInstanceState == null || savedInstanceState.getStringArrayList(NOTICE_CATEGORIES) == null){
            LoadNoticeDataTask loadTask = new LoadNoticeDataTask(this,noticeId);
            loadTask.execute();
        }
        else{
            noticeCategories = savedInstanceState.getStringArrayList(NOTICE_CATEGORIES);
            mCategory.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, noticeCategories));
            if(category != null){
                mCategory.setSelection(CampusPolitoApplication.getStringItemPosition(noticeCategories, category) + 1);
            }
        }



        mTitleField = (EditText) findViewById(R.id.title);
        mTitleField.setText(title);

        mDescriptionField = (EditText) findViewById(R.id.description);
        mDescriptionField.setText(description);



        mCostField = (EditText) findViewById(R.id.cost);

        mCostField.setText(cost);




        searchTagsFlowLayout = (FlowLayout) findViewById(R.id.search_tags);
        addSearchTagButton = (FloatingActionButton) findViewById(R.id.add_tag);
        tagView = (AutoCompleteTextView) findViewById(R.id.tag);

        ParseQuery<Notice> query = ParseQuery.getQuery(Notice.class);
        query.selectKeys(Arrays.asList(Notice.TAGS));

        final Activity thisActivity = this;
        query.findInBackground(new FindCallback<Notice>() {
            @Override
            public void done(List<Notice> list, ParseException e) {
                if(e != null){
                    tagView.setAdapter(null);
                    Log.e(TAG, "Error while fetching tags from Notice objects",e);
                }
                else {
                    List<String> tagsAvailable = new ArrayList<String>();
                    if(list != null){
                        for(Notice n : list){
                            List<String> noticeTags = n.getTags();
                            if(noticeTags != null){
                                for(String tag : noticeTags){
                                    if(!tagsAvailable.contains(tag))
                                        tagsAvailable.add(tag);
                                }

                            }
                        }
                    }
                    if(!tagsAvailable.isEmpty())
                        Collections.sort(tagsAvailable);

                    tagView.setAdapter(new ArrayAdapter<String>(thisActivity,
                            android.R.layout.simple_list_item_1,tagsAvailable));
                }
            }
        });

        noTagsText = (TextView) findViewById(R.id.no_tags);
        uploadButton = (Button) findViewById(R.id.upload);
        photosStaticGridView = (LinearLayout) findViewById(R.id.photosGridView);
        

        for(String tag : tagList)
            addTagToFlowLayout(tag,getLayoutInflater(),searchTagsFlowLayout,true);


        DynamicLinearLayoutViewInflater.getInstance().addBitmapViewsToLayout(pictureList, thisActivity, photosStaticGridView, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent fullscreenIntent = new Intent(thisActivity, FullscreenImageActivity.class);
                fullscreenIntent.putExtra(FullscreenImageActivity.INTENT_IMAGE_URL, (String) v.getTag(R.id.photosGridView));
                startActivity(fullscreenIntent);
            }
        }, R.id.photosGridView);
       
        mCreateNotice.setOnClickListener(this);

        uploadButton.setOnClickListener(this);

        addSearchTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tagView.getText() != null && !TextUtils.isEmpty(tagView.getText())){
                    tagString = Notice.generateValidTag(tagView.getText().toString());

                    if (!tagList.contains(tagString) && !tagString.isEmpty()) {
                        tagList.add(tagString);
                        tagView.setText("");
                        addTagToFlowLayout(tagString, getLayoutInflater(), searchTagsFlowLayout, false);
                    }
                }

            }
        });
    }

    private void addTagToFlowLayout(String tag, LayoutInflater inflater, ViewGroup container, final boolean silent) {
        View t = inflater.inflate(R.layout.tag_layout, container, false);
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 2, 2, 2);
        t.setLayoutParams(params);

        TextView tagText = (TextView) t.findViewById(R.id.tag);
        tagText.setText(tag);
        t.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ViewGroup parentView = (ViewGroup) v.getParent();
                TextView t = (TextView) v.findViewById(R.id.tag);
                int index = tagList.indexOf(t.getText().toString());

                if (index > -1 && index < tagList.size()) {
                    tagList.remove(index);
                    parentView.removeView(v);
                    if (!silent)
                        Snackbar.make(mCreateNotice, getString(R.string.tag_removed), Snackbar.LENGTH_SHORT).show();

                }
                if (tagList.size() == 0)
                    noTagsText.setVisibility(View.VISIBLE);
                return true;
            }
        });
        searchTagsFlowLayout.addView(t);
        if(!silent)
            Snackbar.make(mCreateNotice, getString(R.string.tag_added), Snackbar.LENGTH_SHORT).show();
        if (noTagsText.getVisibility() == View.VISIBLE)
            noTagsText.setVisibility(View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(NOTICE_ID,noticeId);
        outState.putStringArrayList(TAGS_LIST, tagList);
        outState.putStringArrayList(PICTURES_LIST, pictureList);
        outState.putParcelableArrayList(NEW_PICTURE_LIST, newPictureList);

        outState.putString(TITLE, mTitleField.getText().toString());

        outState.putString(DESCRIPTION, mDescriptionField.getText().toString());

        outState.putString(COST, mCostField.getText().toString());

        outState.putString(CATEGORY, (String)mCategory.getSelectedItem());

        if(noticeCategories != null)
            outState.putStringArrayList(NOTICE_CATEGORIES, noticeCategories);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(noticeId != null)
            getMenuInflater().inflate(R.menu.menu_create_notice,menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        if(id == R.id.action_delete_notice){
            deleteNotice();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_notice_button:
                saveNotice();
                break;

            case R.id.upload:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void deleteNotice() {

        DeleteNoticeTask task = new DeleteNoticeTask(this,noticeId);
        task.execute();
    }

    public class DeleteNoticeTask extends AsyncTask<Void,Void,Boolean> {

        private final Context ctx;
        private final String noticeId;

        public DeleteNoticeTask(Context ctx, String noticeId){
            this.ctx = ctx;
            this.noticeId = noticeId;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                Notice notice;

                ParseQuery<Notice> parseQuery = ParseQuery.getQuery(Notice.class);
                notice = parseQuery.get(noticeId);

                if(notice == null)
                    return false;
                
                Student profile = notice.getAdvertiser();
                profile.removeNotice(notice);

                notice.delete();

                profile.save();

                return true;
            }
            catch (ParseException e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if(aBoolean){
                setResult(ShowNoticeDetailsActivity.NOTICE_DELETED);
                finish();
            }
            else{
                showProgress(false);
                Snackbar.make(mCreateNotice, getString(R.string.parse_generic_error),Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void saveNotice() {

        boolean cancel = false;
        View focusView = null;


        String title = mTitleField.getText().toString();
        String description = mDescriptionField.getText().toString();
        String cost = mCostField.getText().toString();
        String category = (String)mCategory.getSelectedItem();


        if(TextUtils.isEmpty(title)){
            mTitleField.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mTitleField;
        }

        if(TextUtils.isEmpty(description)){
            mDescriptionField.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mDescriptionField;
        }


        if(TextUtils.isEmpty(cost)){
            mCostField.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mCostField;
        }else {
            try{
                Float costValue = Float.parseFloat(cost);
            }
            catch (NumberFormatException e){
                mCostField.setError(getString(R.string.error_field_wrong_format));
                cancel = true;
                focusView = mCostField;
            }
        }

        if(category.equals(getString(R.string.prompt_category))){
            mCategory.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mCategory;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else {
            showProgress(true);
            mNoticeTask = new NoticeCreationTask(this,title,description,cost,category,tagList, newPictureList, noticeId);
            mNoticeTask.execute((Void) null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK && null != data) {
            newPictureList.add(data.getData());
            pictureList.add(data.getData().toString());
            final Activity thisActivity = this;
            DynamicLinearLayoutViewInflater.getInstance().addBitmapViewsToLayout(pictureList, thisActivity, photosStaticGridView, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent fullscreenIntent = new Intent(thisActivity, FullscreenImageActivity.class);
                    fullscreenIntent.putExtra(FullscreenImageActivity.INTENT_IMAGE_URL, (String) v.getTag(R.id.photosGridView));
                    startActivity(fullscreenIntent);
                }
            }, R.id.photosGridView);

        }
    }


    /**Private  class used only to exchange progress data with UI Thread**/
    private  class ProgressData {
        public Integer progress;
        public int picNumber;

        public ProgressData(){

        }
        public ProgressData(Integer progress, int picNumber){
            this.progress = progress;
            this.picNumber = picNumber;
        }
    }


    public class LoadNoticeDataTask extends AsyncTask<Void,Void,Boolean> {

        private final Context ctx;
        private final String noticeId;

        private Notice modifiedNotice;

        public LoadNoticeDataTask(Context ctx, String noticeId){
            this.ctx = ctx;
            this.noticeId = noticeId;
        }

        @Override
        protected void onPreExecute() {
            mFormView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                noticeCategories = new ArrayList<String>(ParseApplicationGlobalsQuery.getInstance().getNoticeCategories().getFirst().getNoticeCategories());

                if(noticeId != null){
                    ParseQuery<Notice> query = ParseQuery.getQuery(Notice.class);
                    modifiedNotice = query.get(noticeId);

                    if(modifiedNotice == null)
                        return false;
                    modifiedNotice.fetchIfNeeded();
                }
                return true;
            }
            catch (ParseException e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            mCategory.setAdapter(new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, noticeCategories));
            if(category != null){
                mCategory.setSelection(CampusPolitoApplication.getStringItemPosition(noticeCategories, category) + 1);
            }


            if(modifiedNotice != null){
                mTitleField.setText(modifiedNotice.getTitle());
                mCategory.setSelection(CampusPolitoApplication.getStringItemPosition(noticeCategories, modifiedNotice.getCategory()) + 1);
                mCostField.setText("" + modifiedNotice.getCost());
                mDescriptionField.setText(modifiedNotice.getDescription());

                ArrayList<String> pictureUrlsFromParseFiles = new ArrayList<>();

                if(modifiedNotice.getPhotos() != null)
                    for(ParseFile picture : modifiedNotice.getPhotos()){
                        pictureUrlsFromParseFiles.add(picture.getUrl());
                    }
                pictureList = pictureUrlsFromParseFiles;

                DynamicLinearLayoutViewInflater.getInstance().addBitmapViewsToLayout(pictureList, ctx, photosStaticGridView, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent fullscreenIntent = new Intent(ctx, FullscreenImageActivity.class);
                        fullscreenIntent.putExtra(FullscreenImageActivity.INTENT_IMAGE_URL, (String) v.getTag(R.id.photosGridView));
                        ctx.startActivity(fullscreenIntent);
                    }
                }, R.id.photosGridView);


                tagList = new ArrayList<>(modifiedNotice.getTags());
                for(String tag : tagList)
                    addTagToFlowLayout(tag,getLayoutInflater(),searchTagsFlowLayout,true);


            }


            mFormView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.GONE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class NoticeCreationTask extends AsyncTask<Void, ProgressData, Boolean> {

        final String title;
        final String description;
        final String cost;
        final String category;
        final List<String> tags;
        final List<Uri> pictures;

        String noticeId;
        Context ctx;

        ProgressDialog barProgressDialog;



        NoticeCreationTask(Context ctx, String title, String description,String cost,
                           String category,
                           List<String> tags, List<Uri> pictures, String noticeId) {
            this.title = title;
            this.description = description;
            this.cost = cost;
            this.tags = tags;
            this.pictures = pictures;
            this.category = category;
            this.noticeId = noticeId;
            this.ctx = ctx;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            barProgressDialog = new ProgressDialog(ctx);

            barProgressDialog.setTitle(getString(R.string.upload_image_title));
            barProgressDialog.setMessage(getString(R.string.upload_image_msg));
            barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            barProgressDialog.setProgress(0);
            barProgressDialog.setMax(100);


        }

        @Override
        protected void onProgressUpdate(ProgressData... values) {
            super.onProgressUpdate(values);
            ProgressData progress = values[0];
            if(!barProgressDialog.isShowing())
                barProgressDialog.show();


            barProgressDialog.setProgress(progress.progress);

            if(progress.progress == 100){
                if(progress.picNumber > pictures.size()){
                    barProgressDialog.setProgress(0);
                    barProgressDialog.setMax(100);
                    barProgressDialog.setTitle(getString(R.string.upload_image_title)+" ("+(progress.picNumber+1)+")");
                }
            }
        }

        @Override
        protected Boolean doInBackground(final Void... params) {


            try {


                AppUser currentUser = (AppUser)AppUser.getCurrentUser();

                if(currentUser == null || !currentUser.isStudent())
                    return false;

                Log.d(TAG, "Current user is a valid advertiser");

                Student profile = currentUser.getStudentProfile();
                profile.fetchIfNeeded();

                Notice notice;
                if(noticeId == null){
                    notice = new Notice();
                    Log.d(TAG, "Creating new notice.");
                }
                else {

                    ParseQuery<Notice> parseQuery = ParseQuery.getQuery(Notice.class);
                    notice = parseQuery.get(noticeId);

                    Log.d(TAG, "Updating notice " + noticeId);
                }

                notice.setTitle(title.trim());
                notice.setDescription(description.trim());
                notice.setCategory(category);

                notice.setCost(Float.parseFloat(cost));

                Log.d(TAG, "Basic data added to notice.");

                notice.setTags(tags);

                StringBuffer sb = new StringBuffer();
                sb.append(category.toLowerCase());
                if(tags != null){
                    for(String tag : tags){
                        sb.append(Notice.TAGSTRING_SEPARATOR);
                        sb.append(tag);
                    }

                }

                notice.setTagString(sb.toString());

                Log.d(TAG, "Tags added to notice.");
                ByteArrayOutputStream outputStream = null;
                Bitmap bitmapObject;
                ParseFile image = null;
                final ProgressData progressData = new ProgressData();


                if(pictures != null){

                    int picNumber = 0;

                    for (Uri pictureUri : pictures) {
                        //The file upload shall continue if a single file fails to be loaded
                        try {
                            picNumber++;
                            Log.d(TAG, "Processing picture n. "+picNumber);

                            bitmapObject = CampusPolitoApplication.decodeSampledBitmapFromUri(pictureUri,
                                    CampusPolitoApplication.SCALE_SIZE + 1,
                                    CampusPolitoApplication.SCALE_SIZE + 1);


                            bitmapObject = CampusPolitoApplication.scaleBitmapAndKeepRatio(bitmapObject,
                                    CampusPolitoApplication.SCALE_SIZE,
                                    CampusPolitoApplication.SCALE_SIZE);


                            Log.d(TAG, "Bitmap object saved from content provider.");

                            outputStream = new ByteArrayOutputStream();
                            bitmapObject.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                            Log.d(TAG, "Bitmap object compressed to output stream (size " + outputStream.size() + ")");
                            image = new ParseFile("Photo_"+picNumber+".jpg", outputStream.toByteArray());

                            outputStream.close();
                            Log.d(TAG, "Image loaded to Parse File object, starting upload...");


                            final int finalPicNumber = picNumber;

                            Task<Void> saveTask = image.saveInBackground(new ProgressCallback() {
                                @Override
                                public void done(Integer integer) {
                                    progressData.picNumber = finalPicNumber;
                                    progressData.progress = integer;
                                    publishProgress(progressData);
                                }
                            });

                            saveTask.waitForCompletion();


                            if (saveTask.isCompleted()) {
                                Log.d(TAG, "Picture saved to Parse.com");
                                notice.addPhoto(image);
                            }
                        } catch (FileNotFoundException e) {
                            Log.d(TAG, "Picture not found on device.", e);
                        } catch (IOException e) {
                            Log.d(TAG, "An error occurred with input or output stream.", e);
                        } catch (InterruptedException e) {
                            Log.d(TAG, "Save of picture interrupted.", e);
                        } finally {

                            try {
                                if (outputStream != null)
                                    outputStream.close();
                            } catch (IOException e) {
                                Log.d(TAG, "Output stream already closed.");
                            }

                        }
                    }
                }
                boolean isNew = (notice.getAdvertiser() == null);

                if(isNew){
                    Log.d(TAG, "New notice is going to be saved soon...");
                    notice.setAdvertiser(profile);
                }

                notice.save();
                Log.d(TAG, "Notice saved.");

                if(isNew){
                    Log.d(TAG, "Advertiser profile is being attached to the notice");
                    profile.addNotice(notice);
                    Log.d(TAG, "Saving advertiser profile");
                    profile.save();
                }

                Log.i(TAG, "NoticeCreationTask completed.");
                return true;
            }
            catch (NumberFormatException e ){
                Log.e(TAG, "Unable to parse number as float.",e);
                return false;
            }
            catch (ParseException e) {
                Log.e(TAG, "Unable to perform remote login on Parse.com",e);
                return false;
            }

            catch (Exception e){
                Log.e(TAG, "Generic error occurred.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mNoticeTask = null;
            showProgress(false);
            barProgressDialog.hide();

            setResult(ShowNoticeDetailsActivity.NOTICE_UPDATED);
            finish();

        }

        @Override
        protected void onCancelled() {
            mNoticeTask= null;
            showProgress(false);
            barProgressDialog.hide();
        }
    }

}
