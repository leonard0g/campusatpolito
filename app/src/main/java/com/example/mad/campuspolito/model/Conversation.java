package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mdb on 22/07/15.
 */
@ParseClassName("Conversation")
public class Conversation extends ParseObject{

    public static final String USERS = "users";

    public static final String OPT_GRP_NAME = "name";
    public static final String OPT_GRP_DESC = "description";
    public static final String OPT_GRP_ADMINISTRATOR = "administrator";

    public static final String LAST_MSG = "last_msg";

    public Conversation(){

    }

    public void setGroupName(String name){
        put(OPT_GRP_NAME, name);
    }
    public String getGroupName(){
        return getString(OPT_GRP_NAME);
    }
    public void setGroupDescription(String description){
        put(OPT_GRP_DESC, description);
    }
    public String getGroupDescription(){
        return getString(OPT_GRP_DESC);
    }
    public void setUsers(List<AppUser> users){
        put(USERS, users);
    }
    public List<AppUser> getUsers(){
        return getList(USERS);
    }
    public void addUser(AppUser user){
        addUnique(USERS, user);
    }
    public void removeUser(AppUser user){
        removeAll(USERS, Arrays.asList(user));
    }
    public boolean isGroupChat() { return getUsers().size() > 2; }
    public void setLastMsg(String lastMsg){
        put(LAST_MSG,lastMsg);
    }
    public void setGroupAdministrator(AppUser groupAdministrator){
        put(OPT_GRP_ADMINISTRATOR, groupAdministrator);
    }
    public AppUser getGroupAdministrator(){
        return (AppUser)getParseUser(OPT_GRP_ADMINISTRATOR);
    }

    public String getLastMsg(){
        return getString(LAST_MSG);
    }


    /** Utility method used to update conversation object and its 'updated at' field,
     * it also saves the last message to quick display
     *
     * @param message The last message
     */
    public void update(Message message){
        put(LAST_MSG, message.getData());
    }

    /**
     * Utility method used to retrieve recipient user in messages 1to1
     * @return The AppUser object, null if group chat
     */
    public AppUser getRecipientUser(){
        if(isGroupChat())
            return null;

        if(getUsers().get(0).getObjectId().equals(AppUser.getCurrentUser().getObjectId()))
            return getUsers().get(1);
        else
            return getUsers().get(0);

    }

}
