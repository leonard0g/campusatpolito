package com.example.mad.campuspolito.common;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.mad.campuspolito.R;

import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Location;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.model.Teacher;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * @author kurado
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class TimetableListFragment extends ListFragment implements ParseQueryAdapter.OnQueryLoadListener<Lecture> {

    private static final String TAG = TimetableListFragment.class.getName();
    private static final String LIST_STATE = "list_state";

    private OnFragmentInteractionListener mListener;

    private LinearLayout mProgress;
    private FrameLayout mContent;

    private List<Lecture> lectureList;
    LecturesAdapter lecturesAdapter;

    private String courseId;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TimetableListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.lecture_list,container,false);

        mProgress = (LinearLayout)v.findViewById(R.id.list_progress);
        mContent = (FrameLayout)v.findViewById(R.id.content);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null && savedInstanceState.getString(Lecture.COURSE)!=null){
            courseId = savedInstanceState.getString(Lecture.COURSE);
        }
        else
            courseId = mListener.onCourseIdRetrieved();

        lecturesAdapter = new LecturesAdapter(getActivity(), new ParseQueryAdapter.QueryFactory<Lecture>() {
            @Override
            public ParseQuery<Lecture> create() {

                List<Course> courseList = new ArrayList<>();

                try{
                    if(((AppUser)AppUser.getCurrentUser()).isStudent()){
                        Student student = ((AppUser)AppUser.getCurrentUser()).getStudentProfile();
                        student.fetchIfNeeded();
                        if(student.getCourses() != null && student.getCourses().size() > 0){
                            if(courseId!=null){
                                Course course = ParseQuery.getQuery(Course.class).get(courseId);
                                courseList.add(course);
                            }else
                                courseList.addAll(student.getCourses());
                        }




                    }else if(((AppUser)AppUser.getCurrentUser()).isTeacher()){
                        Teacher teacher = ((AppUser)AppUser.getCurrentUser()).getTeacherProfile();
                        teacher.fetchIfNeeded();
                        if(teacher.getCourses() != null && teacher.getCourses().size() > 0){
                            if(courseId!=null){
                                Course course = ParseQuery.getQuery(Course.class).get(courseId);
                                courseList.add(course);
                            }
                            else
                                courseList.addAll(teacher.getCourses());
                        }

                    }

                    ParseObject.fetchAllIfNeeded(courseList);
                }
                catch (ParseException e){
                    Log.e(TAG, "Error while fetching courses from profile object.",e);
                }


                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd",Locale.getDefault());
                ParseQuery<Lecture> parseQuery = ParseQuery.getQuery(Lecture.class);


                parseQuery.whereContainedIn(Lecture.COURSE, courseList);
                parseQuery.whereGreaterThanOrEqualTo(Lecture.DATE, dateFormat.format(Calendar.getInstance().getTime()));
                parseQuery.orderByAscending(Lecture.TIME);

                return parseQuery;

            }
        });

        lecturesAdapter.addOnQueryLoadListener(this);
        setListAdapter(lecturesAdapter);

        if(mListener!=null){
            mListener.onRetrieveAdapter(lecturesAdapter);
            Log.d(TAG,"Adapter successfully retrieved");
        }else{
            Log.e(TAG,"Exception raised by mListener in the onCreate method");
            throw new RuntimeException("mListener can't be null during onCreate method");
        }

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(courseId!=null){
            outState.putString(Lecture.COURSE,courseId);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(!(activity instanceof OnFragmentInteractionListener)){
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mListener = (OnFragmentInteractionListener)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onLectureSelected(lectureList.get(position).getObjectId());
        }
    }

    @Override
    public void onLoading() {
        Log.d(TAG,"Loading lectures...");
        mProgress.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);
    }

    @Override
    public void onLoaded(List<Lecture> list, Exception e) {
        Log.d(TAG, "Lectures loaded");
        mProgress.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);

        if(e==null){
            lectureList = list;
        }else{
            Log.e(TAG,"An error occurred while loading lectures form Parse",e);
            Snackbar.make(mContent,getString(R.string.parse_loading_error),Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void onLectureSelected(String id);
        void onRetrieveAdapter(LecturesAdapter adapter);
        String onCourseIdRetrieved();
    }

}
