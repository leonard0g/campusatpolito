package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;


/**
 * Created by lg on 04/08/15.
 */
@ParseClassName("Location")
public class Location extends ParseObject {
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String LOCATION = "location";
    public static final String BUILDING = "building";
    public static final String FLOOR = "floor";
    public static final String SITE = "site";
    public static final String ORGANIZATION = "organization";
    public static final String TYPE = "type";
    public static final String DETAILS = "details";

    /**
     * Default zero-argument constructor required by Parse API
     */
    public Location(){

    }

    public double getLat(){
        return getDouble(LAT);
    }

    public double getLng(){
        return getDouble(LNG);
    }

    public String getLocation(){
        return getString(LOCATION);
    }

    public String getBuilding(){
        return getString(BUILDING);
    }

    public String getFloor(){
        return getString(FLOOR);
    }

    public String getSite(){
        return getString(SITE);
    }

    public String getOrganization(){
        return getString(ORGANIZATION);
    }

    public String getType(){
        return getString(TYPE);
    }

    public String getDetails(){
        return getString(DETAILS);
    }
}
