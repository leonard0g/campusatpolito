package com.example.mad.campuspolito.student;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.model.Student;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by mdb on 18/08/15.
 */
public class NoticesFragment extends Fragment implements AbsListView.OnItemClickListener, MenuItem.OnMenuItemClickListener, PopupMenu.OnMenuItemClickListener, ParseQueryAdapter.OnQueryLoadListener<Notice> {

    private static final String TAG = NoticesFragment.class.getName();
    private static final String QUERY = "QUERY";


    private ProgressBar queryProgress;
    private LinearLayout content;

    public static final int STUDENT_FAVOURITES = 0;
    public static final int STUDENT_SEARCH = 1;


    private static final String SORT_PARAM = "SORT_PARAM";
    private static final String SORT_DATE_PARAM = "createdAt";
    private static final String SORT_COST_PARAM = "cost";
    private static final String SORT_TAG_PARAM = "tags";

    private static final String SORT_ORDER = "SORT_ORDER";
    private static final int SORT_ASC = 0;
    private static final int SORT_DESC = 1;

    private String mSortParam;
    private int mSortOrder;
    private String mTagParam;




    private static NoticesFragment instance = null;

    private int mQuery;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private NoticesAdapter mAdapter;


    public static NoticesFragment newInstance(int query, String tag, boolean resetQuery) {
        NoticesFragment fragment = new NoticesFragment();
        Bundle args = new Bundle();

        args.putInt(QUERY, query);

        if(tag != null){
            args.putString(SORT_TAG_PARAM, tag.toLowerCase());
        }

        if(instance != null){
            if (!args.containsKey(SORT_TAG_PARAM))
                args.putString(SORT_TAG_PARAM, instance.mTagParam);
            args.putInt(QUERY, query);
            args.putString(SORT_PARAM, instance.mSortParam);
            args.putInt(SORT_ORDER, instance.mSortOrder);
        }


        if(resetQuery){
            Log.d(TAG, "Query reset requested.");
            args.remove(SORT_TAG_PARAM);

        }


        fragment.setArguments(args);
        instance = fragment;
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NoticesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mQuery = getArguments().getInt(QUERY);
            mSortParam = getArguments().getString(SORT_PARAM, SORT_DATE_PARAM);
            mSortOrder = getArguments().getInt(SORT_ORDER, SORT_DESC);
            mTagParam = getArguments().getString(SORT_TAG_PARAM,null);
        }


        performQuery(mSortParam, mSortOrder);

        setHasOptionsMenu(true);
    }


    private void performQuery(String sortParam, int sortOrder){


        mSortParam = sortParam;
        mSortOrder = sortOrder;


        final Student profile = ((AppUser) AppUser.getCurrentUser()).getStudentProfile();

        try {
            profile.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        switch (mQuery){
            case STUDENT_SEARCH:
                Log.d(TAG, "Performing student search query ("+mSortParam+")");
                Log.d(TAG, "Additional tag param is : " + mTagParam);

                mAdapter =
                        new NoticesAdapter(getActivity(), new NoticesAdapter.QueryFactory<Notice>() {
                            public ParseQuery<Notice> create() {
                                // Here we can configure a ParseQuery to our heart's desire.
                                ParseQuery<Notice> query = ParseQuery.getQuery(Notice.class);

                                if(mSortOrder == SORT_ASC)
                                    query.orderByAscending(mSortParam);
                                else
                                    query.orderByDescending(mSortParam);

                                if(mTagParam != null && !TextUtils.isEmpty(mTagParam)){
                                    query.whereContains(Notice.TAGSTRING, mTagParam);
                                }

                                return query;
                            }
                        });

                break;


            case STUDENT_FAVOURITES:
                Log.d(TAG,"Performing student favourites query ("+mSortParam+")");
                final List<String> favouriteNoticesIds = new ArrayList<>();
                Log.d(TAG, "Favourite notices number : " + (profile.getFavouriteNotices() != null? profile.getFavouriteNotices().size() : "null"));

                if(profile.getFavouriteNotices() != null){
                    for(Notice c : profile.getFavouriteNotices())
                        favouriteNoticesIds.add(c.getObjectId());
                }

                mAdapter =
                        new NoticesAdapter(getActivity(), new NoticesAdapter.QueryFactory<Notice>() {
                            public ParseQuery<Notice> create() {
                                // Here we can configure a ParseQuery to our heart's desire.
                                ParseQuery<Notice> query = ParseQuery.getQuery(Notice.class);
                                query.whereContainedIn("objectId", favouriteNoticesIds);


                                if(mSortOrder == SORT_ASC)
                                    query.orderByAscending(mSortParam);
                                else
                                    query.orderByDescending(mSortParam);


                                return query;
                            }
                        });
                break;
        }



        if(mAdapter != null){
            mAdapter.setTextKey(Notice.TITLE);
            mAdapter.addOnQueryLoadListener(this);
        }

        if(mListView != null)
            mListView.setAdapter(mAdapter);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notices_list, container, false);

        queryProgress = (ProgressBar) view.findViewById(R.id.list_progress);
        content = (LinearLayout) view.findViewById(R.id.list_content);

        mListView = (AbsListView) view.findViewById(android.R.id.list);

        mListView.setEmptyView(view.findViewById(android.R.id.empty));
        // Set the adapter
        if(mAdapter.getCount() == 0)
            setEmptyText(getString(R.string.no_results));

        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);



        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        ((NoticesListActivity)getActivity()).onShowNoticeRequested(((Notice)mAdapter.getItem(position)).getObjectId());
     }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(QUERY, mQuery);
        outState.putString(SORT_PARAM, mSortParam);
        outState.putInt(SORT_ORDER, mSortOrder);
        outState.putString(SORT_TAG_PARAM, mTagParam);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_sort).setVisible(true).setCheckable(true);
        menu.findItem(R.id.action_sort).setOnMenuItemClickListener(this);
    }

    public void setEmptyText(CharSequence emptyText) {
        Log.d(TAG, "Setting empty view.");
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof LinearLayout) {
            TextView textView = (TextView) emptyView.findViewById(R.id.text);
            textView.setText(emptyText);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    public void unsetEmptyText(){
        View emptyView = mListView.getEmptyView();

        if(emptyView instanceof  LinearLayout) {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()){


            case R.id.action_sort:
                PopupMenu popup = new PopupMenu(getActivity(), getActivity().findViewById(R.id.action_sort));
                popup.setOnMenuItemClickListener(this);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.sort_notices_options, popup.getMenu());
                popup.show();
                break;

            case R.id.action_sort_date_desc:
                performQuery(SORT_DATE_PARAM,SORT_DESC);
                break;
            case R.id.action_sort_cost_desc:
                performQuery(SORT_COST_PARAM,SORT_DESC);
                break;


            case R.id.action_sort_date_asc:
                performQuery(SORT_DATE_PARAM,SORT_ASC);
                break;
            case R.id.action_sort_cost_asc:
                performQuery(SORT_COST_PARAM,SORT_ASC);
                break;

            default:
                return false;
        }
        return true;
    }

    @Override
    public void onLoading() {
        queryProgress.setVisibility(View.VISIBLE);
        content.setVisibility(View.GONE);

    }

    @Override
    public void onLoaded(List<Notice> list, Exception e) {

        queryProgress.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);


        if(e != null)
            Log.d(TAG,"Error while loading list of notices.",e);
        else{
            Log.d(TAG, "List of notices loaded.");
        }

    }

    public void updateList(){
        if(mAdapter != null)
            mAdapter.loadObjects();
    }
}
