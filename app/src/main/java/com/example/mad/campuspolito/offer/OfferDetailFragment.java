package com.example.mad.campuspolito.offer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.ProfileActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Candidature;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Offer;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.wefika.flowlayout.FlowLayout;

import org.json.JSONException;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by lg on 11/08/15.
 */
public class OfferDetailFragment extends Fragment {
    public static final String OFFER_ID = "offer_id";
    private static final String TAG = OfferDetailFragment.class.getName();

    private String offerId;

    private Toolbar toolbar;
    private SwitchCompat company_toggle;
    private SwitchCompat student_toggle;
    private TextView about;
    private TextView no_requirements;
    private TextView company;
    private TextView job_type;
    private TextView duration;
    private TextView location;
    private TextView category;
    private TextView offer_id;
    private TextView publication_date;
    private TextView expiry_date;
    private FloatingActionButton button;
    private LinearLayout progress_bar;
    private NestedScrollView offer_content;
    private Company c;
    private Student s;
    private Candidature existingCandidature;
    private CardView mCandidaturesCard;
    private LinearLayout mCandidaturesLayout;
    private TextView mRemoveCandidatureHint;
    private TextView mDiscardedCandidature;
    private boolean candidatureDiscarded;
    private FlowLayout requirementsTags;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        offerId = null;
        if (savedInstanceState == null) {
            if (getArguments().containsKey(OFFER_ID)) {
                Log.i(TAG, "onCreate, savedInstanceState is null");

                offerId = getArguments().getString(OFFER_ID);
                Log.i(TAG, "offer id = " + offerId);
            }
        } else {
            Log.i(TAG, "onCreate, restoring instance state");
            offerId = savedInstanceState.getString(OFFER_ID);
            Log.i(TAG, "offer id = " + offerId);
        }

        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView;

        if(offerId == null){
            rootView = inflater.inflate(R.layout.empty_layout, container, false);
        }

        else {
            rootView = inflater.inflate(R.layout.fragment_offer_detail, container, false);

            toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
            company_toggle = (SwitchCompat) rootView.findViewById(R.id.company_toggle);
            student_toggle = (SwitchCompat) rootView.findViewById(R.id.student_toggle);
            about = (TextView) rootView.findViewById(R.id.about);
            no_requirements= (TextView) rootView.findViewById(R.id.no_requirements);
            requirementsTags = (FlowLayout) rootView.findViewById(R.id.requirementsTags);
            company = (TextView) rootView.findViewById(R.id.company);
            job_type = (TextView) rootView.findViewById(R.id.job_type);
            duration = (TextView) rootView.findViewById(R.id.duration);
            location = (TextView) rootView.findViewById(R.id.location);
            category = (TextView) rootView.findViewById(R.id.category);
            offer_id = (TextView) rootView.findViewById(R.id.offer_id);
            publication_date = (TextView) rootView.findViewById(R.id.publication_date);
            expiry_date = (TextView) rootView.findViewById(R.id.expiry_date);
            button = (FloatingActionButton) rootView.findViewById(R.id.button);
            progress_bar = (LinearLayout) rootView.findViewById(R.id.progress_bar);
            offer_content = (NestedScrollView) rootView.findViewById(R.id.offer_content);
            mCandidaturesCard = (CardView) rootView.findViewById(R.id.candidatures_card);
            mCandidaturesLayout = (LinearLayout) rootView.findViewById(R.id.candidatures);
            mRemoveCandidatureHint = (TextView) rootView.findViewById(R.id.remove_candidature_hint);
            mDiscardedCandidature = (TextView) rootView.findViewById(R.id.discarded_banner);

            progress_bar.setVisibility(View.VISIBLE);
            offer_content.setVisibility(View.GONE);
            button.setVisibility(View.GONE);

            contentSetup();
        }

        return rootView;
    }

    private void contentSetup(){

        final DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getActivity());

        getOffer(offerId, new GetCallback<Offer>() {
            @Override
            public void done(Offer offer, ParseException e) {
                progress_bar.setVisibility(View.GONE);
                offer_content.setVisibility(View.VISIBLE);

                if (e == null) {
                    ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(offer.getTitle());

                    final AppUser user = (AppUser) AppUser.getCurrentUser();
                    if (user != null) {
                        if (user.isStudent()) {
                            setupStudent(offer);
                        } else { //user.isCompany()
                            try {
                                c = user.getCompanyProfile();
                                c.fetchIfNeeded();
                                setupCompany(offer);
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                    try {
                        about.setText(offer.getDescription());

                        if(offer.getRequirements()==null || offer.getRequirements().size()==0){
                            no_requirements.setVisibility(View.VISIBLE);
                        }else{
                            for (String s : offer.getRequirements()) {
                                addTagToFlowLayout(s, requirementsTags);
                            }
                        }



                        job_type.setText(offer.getJobType());
                        duration.setText(offer.getDuration().getInt(Offer.DURATION_PERIOD_LENGTH) + " " + offer.getDuration().getString(Offer.DURATION_PERIOD_TYPE));
                        location.setText(offer.getLocationName());
                        category.setText(offer.getCategory());
                        offer_id.setText(offer.getObjectId());
                        if(offer.getPublishedAt() != null) {
                            publication_date.setText(dateFormat.format(offer.getPublishedAt()));
                        }
                        expiry_date.setText(dateFormat.format(offer.getExpireDate()));

                    }catch(JSONException j){
                        //TODO
                        j.printStackTrace();
                    }

                } else {
                    Log.i(TAG, "getOffer error: " + e.getMessage());
                }
            }
        });
    }

    private void addTagToFlowLayout(String tag, FlowLayout tagsContainer) {
        Context ctx=tagsContainer.getContext();
        View t = ((LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tag_layout_inactive,tagsContainer, false);
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 2, 2, 2);
        t.setLayoutParams(params);
        TextView tagText = (TextView) t.findViewById(R.id.tag);
        tagText.setText(tag);
        tagsContainer.addView(t);
    }

    private void setupStudent(final Offer offer){
        try {
            s = ((AppUser) AppUser.getCurrentUser()).getStudentProfile();
            s.fetchIfNeeded();

            c = offer.getCompany();
            c.fetchIfNeeded();
            ((TextView) toolbar.findViewById(R.id.toolbar_subtitle)).setText(c.getName());
            company.setText(c.getName());

            if(s.getFavouriteOffers() != null && s.getFavouriteOffers().contains(offer)){
                student_toggle.setChecked(true);
            }
            else{
                student_toggle.setChecked(false);
            }

            student_toggle.setVisibility(View.VISIBLE);
            student_toggle.setOnCheckedChangeListener(studentToggleListener);

            if (alreadyApplied(offer)) {
                button.setImageResource(R.drawable.ic_clear);
                Snackbar.make(button, getString(R.string.withdraw_candidature), Snackbar.LENGTH_SHORT).show();
            } else {
                button.setImageResource(R.drawable.ic_activate);
                Snackbar.make(button, getString(R.string.send_candidature), Snackbar.LENGTH_SHORT).show();
            }

            if(candidatureDiscarded)
                mDiscardedCandidature.setVisibility(View.VISIBLE);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(alreadyApplied(offer) && existingCandidature != null) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.delete_candidature_question);
                        builder.setMessage(R.string.application_will_be_withdrawn);
                        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Log.i(TAG, "Deleting candidature");
                                offer.removeCandidature(existingCandidature);
                                s.removeCandidature(existingCandidature);

                                offer.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            Log.i(TAG, "Candidature removed from offer");
                                        } else {
                                            Log.i(TAG, "Candidature could not be removed from offer");
                                            e.printStackTrace();
                                        }
                                    }
                                });

                                s.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            Log.i(TAG, "Candidature removed from student");
                                        } else {
                                            Log.i(TAG, "Candidature could not be removed from student");
                                            e.printStackTrace();
                                        }
                                    }
                                });

                                ParseObject.deleteAllInBackground(Collections.singletonList(existingCandidature), new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            Log.i(TAG, "Candidature deleted");
                                        } else {
                                            Log.i(TAG, "Candidature could not be deleted");
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                button.setImageResource(R.drawable.ic_activate);
                                Snackbar.make(button, getString(R.string.candidature_deleted), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                        builder.create();
                        builder.show();
                    }

                    else{
                        Log.i(TAG, "Applying for this job offer");
                        Candidature candidature = new Candidature();
                        candidature.setOffer(offer);
                        candidature.setStudent(s);
                        candidature.setStatus(Candidature.STATUS_NEW);

                        offer.addCandidature(candidature);
                        s.addCandidature(candidature);
                        s.addFavouriteOffer(offer);

                        candidature.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Log.i(TAG, "Candidature saved");
                                    button.setImageResource(R.drawable.ic_clear);
                                    Snackbar.make(button, getString(R.string.application_sent), Snackbar.LENGTH_SHORT).show();
                                    if(mDiscardedCandidature.getVisibility() == View.VISIBLE)
                                        mDiscardedCandidature.setVisibility(View.GONE);
                                } else {
                                    Log.i(TAG, "Candidature could not be saved");
                                    Snackbar.make(button, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            });


            button.setVisibility(View.VISIBLE);

            company.setPaintFlags(company.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            company.setTextColor(getResources().getColor(R.color.colorAccent));
            company.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(TAG, "company clicked");
                    try {
                        Intent showCompanyProfile = new Intent(getActivity(), ProfileActivity.class);
                        Company c = offer.getCompany();
                        c.fetchIfNeeded();
                        showCompanyProfile.putExtra(ProfileActivity.APP_USER_OBJECT_ID, c.getUser().getObjectId());
                        showCompanyProfile.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_COMPANY);
                        startActivity(showCompanyProfile);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }catch(ParseException e){
            e.printStackTrace();
        }
    }

    private void setupCompany(final Offer offer){
        ((TextView) toolbar.findViewById(R.id.toolbar_subtitle)).setText(c.getName());
        company.setText(c.getName());
        company_toggle.setVisibility(View.VISIBLE);

        company_toggle.setChecked(offer.getIsPublished()); // placed before setOnChangeListener to prevent triggering it at startup
        company_toggle.setOnCheckedChangeListener(companyToggleListener);

        button.setImageResource(R.drawable.ic_compose);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditOfferActivity.class);
                intent.putExtra(EditOfferActivity.OFFER_ID, offer.getObjectId());
                startActivityForResult(intent, EditOfferActivity.EDIT_REQUEST);
            }
        });
        button.setVisibility(View.VISIBLE);

        FetchCandidaturesData task = new FetchCandidaturesData(getActivity(), offer.getObjectId());
        task.execute();

    }

    private class FetchCandidaturesData extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final String offerId;
        private boolean hasResults;

        public FetchCandidaturesData(Context ctx, String offerId){
            this.ctx = ctx;
            this.offerId = offerId;
            hasResults = false;
        }


        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                ParseQuery<Offer> queryOffer = ParseQuery.getQuery(Offer.class);
                Offer offer = queryOffer.get(offerId);
                if(offer == null)
                    return false;


                ParseQuery<Candidature> query = ParseQuery.getQuery(Candidature.class);
                query.whereEqualTo(Candidature.OFFER, offer);
                query.orderByAscending("createdAt");
                query.include(Candidature.STUDENT);
                query.whereNotEqualTo(Candidature.STATUS, Candidature.STATUS_DISCARDED);

                List<Candidature> candidatureList = query.find();

                if(candidatureList != null && candidatureList.size() > 0){

                    final List<Student> studentsList = new ArrayList<>();
                    for(Candidature c : candidatureList)
                        studentsList.add(c.getStudent());

                    ParseObject.fetchAllIfNeeded(studentsList);


                    if(studentsList.size() > 0)
                        hasResults = true;

                    DynamicLinearLayoutViewInflater.addStudentViewsToLayout(studentsList, ctx, mCandidaturesLayout, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent profileIntent = new Intent(ctx,ProfileActivity.class);
                            profileIntent.putExtra(ProfileActivity.APP_USER_OBJECT_ID,(String)v.getTag(R.id.candidatures));
                            profileIntent.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_STUDENT);
                            startActivity(profileIntent);
                        }
                    }, new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            String studentUserId = (String)v.getTag(R.id.candidatures);
                            DiscardCandidatureTask task = new DiscardCandidatureTask(ctx, studentUserId, offerId);
                            task.execute();
                            mCandidaturesLayout.removeView(v);

                            return true;
                        }
                    }, R.id.candidatures);
                }

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching candidatures data for company.",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                if(hasResults)
                    mRemoveCandidatureHint.setVisibility(View.VISIBLE);

                mCandidaturesCard.setVisibility(View.VISIBLE);
            }
        }
    }

    private class DiscardCandidatureTask extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final String studentUserId;
        private final String offerId;

        public DiscardCandidatureTask(Context ctx, String studentUserId, String offerId){
            this.ctx = ctx;
            this.studentUserId = studentUserId;
            this.offerId = offerId;
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                ParseQuery<AppUser> userParseQuery = ParseQuery.getQuery(AppUser.class);
                AppUser user = userParseQuery.get(studentUserId);

                if(user == null){
                    Log.e(TAG, "No user found for student user id : " + studentUserId);
                    return false;
                }

                user.fetchIfNeeded();

                Student student = user.getStudentProfile();

                if(student == null){
                    Log.e(TAG, "The retrieved user is not a student.");
                    return false;
                }
                ParseQuery<Offer> OfferQuery = ParseQuery.getQuery(Offer.class);
                Offer offer = OfferQuery.get(offerId);

                ParseQuery<Candidature> query = ParseQuery.getQuery(Candidature.class);
                query.whereEqualTo(Candidature.STUDENT, student);
                query.whereEqualTo(Candidature.OFFER, offer);
                query.whereNotEqualTo(Candidature.STATUS, Candidature.STATUS_DISCARDED);

                List<Candidature> cResults = query.find();
                if(cResults == null || cResults.size() == 0){
                    Log.e(TAG, "No candidature found with given student user id and offer id, and still not discarded");
                    return false;
                }
                Candidature removedCandidature = cResults.get(0);

                removedCandidature.setStatus(Candidature.STATUS_DISCARDED);

                //student.removeCandidature(removedCandidature);
                offer.removeCandidature(removedCandidature);

                offer.save();
                removedCandidature.save();
                return true;

            }
            catch (ParseException e){
                Log.e(TAG, "Error while discarding candidature.",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                if(button != null && isVisible())
                Snackbar.make(button, getString(R.string.remove_candidature_success),Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private boolean alreadyApplied(Offer offer) {
        List<Candidature> candidatureList = s.getCandidatures();

        if(candidatureList != null) {
            for (Candidature candidature : candidatureList) {
                try {
                    candidature.fetchIfNeeded();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (candidature.getOffer().getObjectId().equals(offer.getObjectId()) //check if there exists a non-discarded candidature
                        && candidature.getStatus().equals(Candidature.STATUS_NEW)) {
                        existingCandidature = candidature;
                        candidatureDiscarded = false;
                        return true;
                }
            }
            for(Candidature candidature : candidatureList) { //check if there exists a list a discarded candidature
                if(candidature.getOffer().getObjectId().equals(offer.getObjectId())
                        && candidature.getStatus().equals(Candidature.STATUS_DISCARDED)){
                    existingCandidature = candidature;
                    candidatureDiscarded = true;
                    return false;
                }
            }


        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == EditOfferActivity.EDIT_REQUEST && resultCode == Activity.RESULT_OK){
            Snackbar.make(button, getString(R.string.changes_saved), Snackbar.LENGTH_SHORT).show();
            contentSetup();
        }
    }

    public void getOffer(String objectId, GetCallback<Offer> callback) {
        ParseQuery<Offer> query = ParseQuery.getQuery(Offer.class);
        query.getInBackground(objectId, callback);
    }

    private CompoundButton.OnCheckedChangeListener companyToggleListener = new CompoundButton.OnCheckedChangeListener(){

        @Override
        public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {

        getOffer(offerId, new GetCallback<Offer>() {
            @Override
            public void done(Offer offer, ParseException e) {
                if (e == null) {
                    offer.setIsPublished(isChecked);
                    if(isChecked){
                        offer.setPublishedAt(Calendar.getInstance().getTime());
                    }
                    else{
                        offer.remove(Offer.PUBLISHED_AT);
                    }
                    offer.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                if (isChecked) {
                                    Snackbar.make(company_toggle, getString(R.string.offer_published), Snackbar.LENGTH_SHORT).show();
                                    //update offers list
                                    if(getActivity() instanceof OfferListActivity){
                                        ((OfferListActivity) getActivity()).onUpdateContent();
                                    }
                                } else {
                                    Snackbar.make(company_toggle, getString(R.string.offer_hidden), Snackbar.LENGTH_SHORT).show();
                                    //update offers list
                                    if(getActivity() instanceof OfferListActivity){
                                        ((OfferListActivity) getActivity()).onUpdateContent();
                                    }
                                }
                            } else {
                                Snackbar.make(company, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Log.i(TAG, "getOffer error: " + e.getMessage());
                }
            }
        });
        }
    };


    private CompoundButton.OnCheckedChangeListener studentToggleListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {

        getOffer(offerId, new GetCallback<Offer>() {
            @Override
            public void done(Offer offer, ParseException e) {
            if (e == null) {
                Student student = ((AppUser)AppUser.getCurrentUser()).getStudentProfile();
                if(isChecked){
                    student.addFavouriteOffer(offer);
                    student.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Snackbar.make(student_toggle, getString(R.string.offer_added_to_favourites), Snackbar.LENGTH_SHORT).show();
                                //update offers list
                                if(getActivity() instanceof OfferListActivity){
                                    ((OfferListActivity) getActivity()).onUpdateContent();
                                }
                            } else {
                                Snackbar.make(student_toggle, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
                else{
                    student.removeFavouriteOffer(offer);
                    student.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Snackbar.make(student_toggle, getString(R.string.offer_removed_from_favourites), Snackbar.LENGTH_SHORT).show();
                                //update offers list
                                if(getActivity() instanceof OfferListActivity){
                                    ((OfferListActivity) getActivity()).onUpdateContent();
                                }
                            } else {
                                Snackbar.make(student_toggle, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                Log.i(TAG, "getOffer error: " + e.getMessage());
            }
            }
        });
        }
    };


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(OFFER_ID, offerId);
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OfferDetailFragment() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case R.id.action_delete_offer:
                final Context context = getActivity();
                getOffer(offerId, new GetCallback<Offer>() {
                    @Override
                    public void done(final Offer offer, ParseException e) {
                        if (e == null) {
                            offer.setIsPublished(false);

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(R.string.delete_offer_question);
                            builder.setMessage(R.string.offer_will_be_removed);
                            builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progress_bar.setVisibility(View.VISIBLE);
                                    ParseObject.deleteAllInBackground(Collections.singletonList(offer), new DeleteCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            progress_bar.setVisibility(View.GONE);
                                            if (e == null) {
                                                Log.i(TAG, "Offer deleted");
                                                Company company = ((AppUser)AppUser.getCurrentUser()).getCompanyProfile();
                                                company.removeOffer(offer);
                                                company.saveInBackground(new SaveCallback() {
                                                    @Override
                                                    public void done(ParseException e) {
                                                        if(e == null){
                                                            if(getActivity() instanceof OfferListActivity){
                                                                ((OfferListActivity) getActivity()).onUpdateContent();
                                                                ((OfferListActivity)getActivity()).onOfferSelected(null);
                                                            }
                                                            else{
                                                                Intent returnIntent = new Intent();
                                                                getActivity().setResult(OfferDetailActivity.OFFER_DELETED, returnIntent);
                                                                getActivity().finish();
                                                            }
                                                        }
                                                        else{
                                                            Log.e(TAG, "Exception raised while removing offer from company offers");
                                                            Snackbar.make(toolbar, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });


                                            } else {
                                                Log.e(TAG, "Exception raised while deleting offer");
                                                Snackbar.make(toolbar, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                                                //mProgressBar.setVisibility(View.GONE);
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            });
                            builder.create();
                            builder.show();

                        } else {
                            e.printStackTrace();
                        }
                    }
                });
                break;

            default:
                return false;
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        AppUser user = (AppUser)AppUser.getCurrentUser();
        if(user != null){
            if(user.isCompany()) {
                inflater.inflate(R.menu.menu_delete_offer, menu);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


}
