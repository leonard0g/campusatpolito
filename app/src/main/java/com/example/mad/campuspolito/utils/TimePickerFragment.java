package com.example.mad.campuspolito.utils;


import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.mad.campuspolito.R;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private TimePickerInteraction timePickerInteraction;
    private int Hour;
    private int Minute;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c;
        if(timePickerInteraction!=null){
            c = timePickerInteraction.setTime();
        }else{
            c = Calendar.getInstance();
        }
        Hour = c.get(Calendar.HOUR_OF_DAY);
        Minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, Hour, Minute, true);
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        Hour = hourOfDay;
        Minute = minute;
        if(timePickerInteraction!=null){
            timePickerInteraction.getTime(Hour,Minute);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof TimePickerInteraction){
            timePickerInteraction = (TimePickerInteraction)activity;
        }
        else
            throw new RuntimeException("Activity must implements TimePickerInteraction interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        timePickerInteraction=null;
    }

    public interface TimePickerInteraction{
        Calendar setTime();
        void getTime(int hour,int minute);
    }
}
