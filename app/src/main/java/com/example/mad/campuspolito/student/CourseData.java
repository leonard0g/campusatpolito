package com.example.mad.campuspolito.student;

import android.text.TextUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by mdb on 04/09/15.
 */
public class CourseData implements Serializable{

    private static final long serialVersionUID = 7526471155622776147L;

    private String name;
    private String semester;
    private String code;
    private int cfu;
    private String id;

    public CourseData(String name, String semester, String code,
                      int cfu, String id){
        setId(id);
        setName(name);
        setSemester(semester);
        setCode(code);
        setCfu(cfu);
    }

    public String getName(){
        return name;
    }
    public String getSemester(){
        return semester;
    }
    public String getCode(){
        return code;
    }
    public int getCfu(){
        return cfu;
    }
    public String getId(){
        return id;
    }

    public void setName(String name){
        if(name == null || TextUtils.isEmpty(name))
            this.name = "";
        else
            this.name = name;
    }

    public void setSemester(String semester){
        if(semester == null || TextUtils.isEmpty(semester))
            this.semester = "";
        else
            this.semester = semester;
    }

    public void setCode(String code){
        if(code == null || TextUtils.isEmpty(code)){
            this.code = "";
        }
        else
            this.code = code;
    }

    public void setCfu(int cfu){
        this.cfu = cfu;
    }
    public void setId(String id){
        if(id == null || TextUtils.isEmpty(id))
            throw new RuntimeException("Course id can't be null.");
        this.id = id;
    }

    /**
     * Always treat de-serialization as a full-blown constructor, by
     * validating the final state of the de-serialized object.
     */
    private void readObject(
            ObjectInputStream aInputStream
    ) throws ClassNotFoundException, IOException {
        //always perform the default de-serialization first
        aInputStream.defaultReadObject();
    }

    /**
     * This is the default implementation of writeObject.
     * Customise if necessary.
     */
    private void writeObject(
            ObjectOutputStream aOutputStream
    ) throws IOException {
        //perform the default serialization for all non-transient, non-static fields
        aOutputStream.defaultWriteObject();
    }
}

