package com.example.mad.campuspolito.company;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.HomeActivity;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.common.ProfileActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Candidature;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Offer;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.offer.OfferDetailActivity;
import com.example.mad.campuspolito.offer.OfferDetailFragment;
import com.example.mad.campuspolito.offer.OfferListActivity;
import com.example.mad.campuspolito.student.ConversationListActivity;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CompanyHomeActivity extends HomeActivity {

    private TextView mHomeName;
    private TextView mHomeDescription;
    private TextView mCompanyCategory;
    private TextView mCompanySubcategory;
    private CircularImageView mHomeImage;
    private LinearLayout mLatestCandidatures;
    private LinearLayout mProgress;
    private NestedScrollView mContent;
    private LinearLayout mFavouriteStudents;

    private Company profile;
    private FetchHomeData task;


    @Override
    protected void onSetHomeContentView() {
        setContentView(R.layout.activity_company_home);

        mHomeName = (TextView)findViewById(R.id.home_name);
        mHomeDescription = (TextView)findViewById(R.id.home_description);
        mCompanyCategory = (TextView)findViewById(R.id.company_category);
        mCompanySubcategory = (TextView)findViewById(R.id.company_subcategory);
        mHomeImage = (CircularImageView)findViewById(R.id.home_image);
        mLatestCandidatures = (LinearLayout)findViewById(R.id.latest_candidatures);
        mProgress = (LinearLayout)findViewById(R.id.home_progress);
        mContent = (NestedScrollView)findViewById(R.id.home_content);
        mFavouriteStudents = (LinearLayout)findViewById(R.id.favourite_students);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(false);
    }

    public class FetchHomeData extends AsyncTask<Void,Void,Boolean> {

        private final Context ctx;
        List<Candidature> latestCandidatures;
        List<Student> favouriteStudents;

        public FetchHomeData(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                profile = currentUser.getCompanyProfile();
                profile.fetchIfNeeded();

                Log.d(TAG, "Basic info about company loaded.");

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Basic info about company has been loaded.");
                    return false;
                }

                ParseQuery<Offer> queryOffers = ParseQuery.getQuery(Offer.class);
                queryOffers.whereEqualTo(Offer.COMPANY, profile);

                ParseQuery<Candidature> query = ParseQuery.getQuery(Candidature.class);
                query.whereNotEqualTo(Candidature.STATUS, Candidature.STATUS_DISCARDED);
                query.whereMatchesQuery(Candidature.OFFER, queryOffers);
                query.orderByDescending("createdAt");
                query.include(Candidature.OFFER);
                query.include(Candidature.STUDENT);
                query.setLimit(5);


                latestCandidatures = new ArrayList<Candidature>(query.find());

                Log.d(TAG, "Info about candidatures loaded.");

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Basic info about candidatures loaded.");
                    return false;
                }

                if(profile.getFavouriteStudents() != null){
                    ParseObject.fetchAllIfNeeded(profile.getFavouriteStudents());
                    favouriteStudents = new ArrayList<Student>(profile.getFavouriteStudents());
                }
                else
                    favouriteStudents = new ArrayList<>();

                Log.d(TAG, "Info about favourite students loaded.");

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Basic info about favourite students loaded.");
                    return false;
                }
                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching home data.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){

                mHomeName.setText(currentUser.toString());
                mHomeDescription.setText(profile.getVATNumber());
                mCompanyCategory.setText(profile.getCategory());
                mCompanySubcategory.setText(profile.getSubcategory());
                if(currentUser.getPicture() != null){
                    mHomeImage.setParseFile(currentUser.getPicture());
                    mHomeImage.loadInBackground();
                }

                if(latestCandidatures.size() == 0){
                    mLatestCandidatures.removeAllViews();
                    findViewById(R.id.no_latest_candidatures).setVisibility(View.VISIBLE);
                }
                else{

                    findViewById(R.id.no_latest_candidatures).setVisibility(View.GONE);
                    DynamicLinearLayoutViewInflater.addCandidatureViewsToLayout(latestCandidatures,
                            ctx, mLatestCandidatures, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String offerId = (String)v.getTag(R.id.latest_candidatures);
                                    Intent showOfferDetails = new Intent(ctx, OfferDetailActivity.class);
                                    showOfferDetails.putExtra(OfferDetailFragment.OFFER_ID, offerId);
                                    ctx.startActivity(showOfferDetails);

                                }
                            }, R.id.latest_candidatures);

                }
                if(favouriteStudents.size() == 0){
                    mFavouriteStudents.removeAllViews();
                    findViewById(R.id.no_favourite_students).setVisibility(View.VISIBLE);
                }
                else {
                    findViewById(R.id.no_favourite_students).setVisibility(View.GONE);
                    DynamicLinearLayoutViewInflater.addStudentViewsToLayout(favouriteStudents,
                            ctx, mFavouriteStudents, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent profileIntent = new Intent(ctx, ProfileActivity.class);
                                    profileIntent.putExtra(ProfileActivity.APP_USER_OBJECT_ID, (String) v.getTag(R.id.favourite_students));
                                    profileIntent.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_STUDENT);
                                    startActivity(profileIntent);
                                }
                            }, null, R.id.favourite_students);
                }


            }
            else {
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
                Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_LONG).show();
            }

            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
        }

        @Override
        protected void onCancelled() {
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            Snackbar.make(mContent,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onSubscribeToPushChannels() {

    }

    @Override
    protected boolean OnDrawerMenuItemSelected(int itemId) {
        switch (itemId) {
            case R.id.company_offers:
                Intent jobsIntent = new Intent(this, OfferListActivity.class);
                startActivity(jobsIntent);
                return true;
            case R.id.company_search_applicants:
                Intent searchApplicantIntent = new Intent(this, CandidatesSearchActivity.class);
                startActivity(searchApplicantIntent);
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onInflateHomeMenu(Menu menu) {
    }

    @Override
    protected boolean onHomeMenuItemSelected(int id) {
        return false;
    }

    @Override
    protected void onHomeViewClick(View v) {

    }

    @Override
    protected void onResumeHomeActivity() {

        task = new FetchHomeData(this);
        task.execute();
    }
}
