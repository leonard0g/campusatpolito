package com.example.mad.campuspolito.student;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.HomeActivity;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.common.ShowCourseDetailsActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;

public class CareerPlanActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = CareerPlanActivity.class.getName();
    private static final String DEGREE_TITLE = "DEGREE_TITLE";
    private static final String DEGREE_LEVEL = "DEGREE_LEVEL";
    private static final String COURSES_MAP = "COURSES_MAP";
    private Toolbar mToolbar;
    private LinearLayout mProgress;
    private NestedScrollView mContent;
    private TextView mDegreeTitle;
    private TextView mDegreeLevel;
    private LinearLayout mCourses;
    private FloatingActionButton mCreatePlanButton;

    private String degreeTitle;
    private String degreeLevel;
    private TreeMap<CourseData,Boolean> coursesMap;

    private boolean doFetchOnlineData;


    private FetchCoursesData task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_plan);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mProgress = (LinearLayout)findViewById(R.id.career_plan_progress);
        mContent = (NestedScrollView)findViewById(R.id.career_plan_content);
        mDegreeTitle = (TextView)findViewById(R.id.student_degree_title);
        mDegreeLevel = (TextView)findViewById(R.id.student_degree_level);
        mCourses = (LinearLayout)findViewById(R.id.student_courses);
        mCreatePlanButton = (FloatingActionButton)findViewById(R.id.create_career_plan_button);

        mCreatePlanButton.setOnClickListener(this);

        doFetchOnlineData = true;

        if(savedInstanceState != null){
            degreeTitle = savedInstanceState.getString(DEGREE_TITLE);
            degreeLevel = savedInstanceState.getString(DEGREE_LEVEL);
            coursesMap = (TreeMap<CourseData,Boolean>)savedInstanceState.getSerializable(COURSES_MAP);

            if(degreeTitle != null && degreeLevel != null && coursesMap != null)
                doFetchOnlineData = false;
        }

        task = new FetchCoursesData(this);
        task.execute();

    }


    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DEGREE_TITLE, degreeTitle);
        outState.putString(DEGREE_LEVEL, degreeLevel);
        outState.putSerializable(COURSES_MAP, coursesMap);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.create_career_plan_button){
            CreateCareerPlan createTask = new CreateCareerPlan(this);
            createTask.execute();
        }
    }

    protected void createCourseListWithCheckboxes(final Context ctx){
        DynamicLinearLayoutViewInflater.addCourseViewsWithCheckboxToLayout(
                coursesMap, ctx, mCourses, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!(v instanceof CheckBox)){
                            String courseId = (String)v.getTag(R.id.student_courses);
                            if(courseId != null){
                                Intent showCourseDetails = new Intent(ctx, ShowCourseDetailsActivity.class);
                                showCourseDetails.putExtra(ShowCourseDetailsActivity.COURSE_ID, courseId);
                                startActivity(showCourseDetails);
                            }
                        }
                    }
                },
                new CompoundButton.OnCheckedChangeListener(

                ) {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        String courseId = (String)buttonView.getTag(R.id.student_courses);

                        for(CourseData cd : coursesMap.keySet()){
                            if(cd.getId().equals(courseId))
                                coursesMap.put(cd,isChecked);
                        }
                    }
                },R.id.student_courses);
    }

    public class CreateCareerPlan extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;

        public CreateCareerPlan(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mContent.setVisibility(View.GONE);
            mCreatePlanButton.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                Student profile = ((AppUser)AppUser.getCurrentUser()).getStudentProfile();

                if(profile == null)
                    return false;

                profile.fetchIfNeeded();

                List<String> selectedCourses = new ArrayList<String>();

                for(CourseData cd : coursesMap.keySet()){
                    if(coursesMap.get(cd)) //if selected
                        selectedCourses.add(cd.getId());
                }

                if(selectedCourses.size() != 0){
                    ParseQuery<Course> query = ParseQuery.getQuery(Course.class);
                    query.whereContainedIn("objectId", selectedCourses);

                    List<Course> result = query.find();

                    profile.setCourses(result);

                    List<String> channels = ParseInstallation.getCurrentInstallation().getList("channels");


                    for(String channel : channels){
                        boolean isCourseConnected = false;
                        if(channel.startsWith(CampusPolitoApplication.COURSE_CHANNEL)){
                            for(Course c : result){
                                if(channel.endsWith(c.getObjectId())){
                                    isCourseConnected = true;
                                    break;
                                }
                            }
                            if(!isCourseConnected){
                                Log.d(TAG, "Unsubscribing from channel : " + channel);
                                ParseInstallation.getCurrentInstallation().removeAll("channels", Arrays.asList(channel));
                                ParseInstallation.getCurrentInstallation().save();
                            }
                        }
                    }
                    for(Course course : result){
                        if(!ParseInstallation.getCurrentInstallation().getList("channels").contains(CampusPolitoApplication.COURSE_CHANNEL+course.getObjectId())){
                            ParsePush.subscribeInBackground(CampusPolitoApplication.COURSE_CHANNEL + course.getObjectId()); //Subscribe to push notifications for conversation
                            Log.d(TAG, "Creating new student course channel for " + course.getTitle());
                        }
                    }


                }
                else {
                    profile.setCourses(new ArrayList<Course>());
                }

                profile.save();

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while creating career plan.",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                createCourseListWithCheckboxes(ctx);
                Snackbar.make(mContent,R.string.career_plan_updated, Snackbar.LENGTH_SHORT).show();

            }
            else {
                Snackbar.make(mContent,R.string.parse_generic_error, Snackbar.LENGTH_LONG).show();
            }
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
            mCreatePlanButton.setVisibility(View.VISIBLE);
        }
    }

    public class FetchCoursesData extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;

        public FetchCoursesData(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mContent.setVisibility(View.GONE);
            mCreatePlanButton.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                if(isCancelled())
                    return false;

                if(doFetchOnlineData){
                    Student profile = ((AppUser)AppUser.getCurrentUser()).getStudentProfile();
                    profile.fetchIfNeeded();
                    Degree degree = profile.getDegree();
                    degree.fetchIfNeeded();

                    degreeTitle = degree.getTitle();
                    degreeLevel = degree.getLevel();

                    ParseQuery<Course> query = ParseQuery.getQuery(Course.class);
                    query.whereEqualTo(Course.DEGREE, degree);
                    query.orderByAscending(Course.TITLE);

                    List<Course> courseList = query.find();
                    List<Course> studentCourses = profile.getCourses();

                    coursesMap = new TreeMap<CourseData,Boolean>(new Comparator<CourseData>() {
                        @Override
                        public int compare(CourseData lhs, CourseData rhs) {
                            return lhs.getName().compareTo(rhs.getName());
                        }
                    });

                    CourseData cd;
                    for(Course c : courseList){

                        cd = new CourseData(c.getTitle(),c.getSemester(),
                                c.getCode(),c.getCfu(),c.getObjectId());

                        if(studentCourses != null && studentCourses.contains(c))
                            coursesMap.put(cd,true);
                        else
                            coursesMap.put(cd,false);
                    }
                }

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching courses data.", e);
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if(aBoolean){
                mDegreeTitle.setText(degreeTitle);
                mDegreeLevel.setText(degreeLevel);

                createCourseListWithCheckboxes(ctx);

            }
            else{
                Snackbar.make(mContent,getString(R.string.parse_generic_error),Snackbar.LENGTH_LONG).show();
            }

            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
            mCreatePlanButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
