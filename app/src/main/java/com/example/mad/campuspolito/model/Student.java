package com.example.mad.campuspolito.model;

import android.util.Log;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Student")
public class Student extends ParseObject{

    public static final String ID_NUMBER = "id_number";
    public static final String USER = "user";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String DATE_OF_BIRTH = "dateOfBirth";
    public static final String SHORT_DESC = "short_description" ;
    public static final String AVAILABILITY = "availability";
    public static final String SKILLS = "skills";
    public static final String LANGUAGES = "languages";
    public static final String DEGREE = "degree";
    //public static final String CV = "cv";

    public static final String EDUCATION = "education";
    public static final String EXPERIENCE = "experience";


    public static final String EDU_EXP_TITLE = "title";
    public static final String EDU_EXP_DESC = "desc";
    public static final String EDU_EXP_START_YEAR = "start_year";
    public static final String EDU_EXP_END_YEAR = "end_year";

    public static final String HOBBIES = "hobbies";

    public static final String FAVOURITE_OFFERS = "favouriteOffers";
    public static final String FAVOURITE_COMPANIES = "favouriteCompanies";
    public static final String FAVOURITE_NOTICES = "favouriteNotices";


    public static final String NOTICES = "notices";
    public static final String COURSES = "courses";
    public static final String CANDIDATURES = "candidatures";

    /**Location is defined by the following elements**/
    public static final String GEOPOINT = "geoPoint";
    public static final String PLACE_ID = "placeId";
    public static final String LOCATION_NAME = "location_name";
    public static final String KEYWORDS = "KEYWORDS";

    public static final String IS_PUBLIC = "is_public";




    /**
     * Empty constructor (Parse related)
     */
    public Student(){

    }

    public void setIsPublic(boolean isPublic){
        put(IS_PUBLIC, isPublic);
    }
    public boolean getIsPublic(){
        return getBoolean(IS_PUBLIC);
    }

    public void setIdNumber(String idNumber){
        put(ID_NUMBER, idNumber);
    }
    public String getIdNumber(){
        return getString(ID_NUMBER);
    }
    @Override
    public String toString() {
        return getName() + " " + getSurname();
    }

    public void setName(String name){
        put(NAME, name);
    }
    public String getName(){
        return getString(NAME);
    }

    public void setSurname(String surname){
        put(SURNAME, surname);
    }
    public String getSurname(){
        return getString(SURNAME);
    }


    public void setUser(AppUser user){
        put(USER, user);
    }

    public AppUser getUser(){
        return (AppUser)getParseUser(USER);
    }

    public void setFavouriteNotices(List<Notice> favouriteNotices){
        put(FAVOURITE_NOTICES, favouriteNotices);
    }

    public List<Notice> getFavouriteNotices(){
        return getList(FAVOURITE_NOTICES);
    }
    public void addFavouriteNotice(Notice notice){
        addUnique(FAVOURITE_NOTICES, notice);
    }
    public void removeFavouriteNotice(Notice notice){
        removeAll(FAVOURITE_NOTICES, Arrays.asList(notice));
    }

    public void setDateOfBirth(Date dateOfBirth){
        put(DATE_OF_BIRTH, dateOfBirth);
    }
    public Date getDateOfBirth(){
        return getDate(DATE_OF_BIRTH);
    }

    public void setGeoPoint(ParseGeoPoint geoPoint){
        put(GEOPOINT, geoPoint);
    }
    public ParseGeoPoint getGeoPoint(){
        return getParseGeoPoint(GEOPOINT);
    }

    public void setPlaceId(String placeId){
        put(PLACE_ID, placeId);
    }
    public String getPlaceId(){
        return getString(PLACE_ID);
    }

    public void setLocationName(String locationName){
        put(LOCATION_NAME,locationName);
    }
    public String getLocationName(){
        return getString(LOCATION_NAME);
    }

    public String getAvailability(){
        return getString(AVAILABILITY);
    }
    public void setAvailability(String availability){
        put(AVAILABILITY, availability);
    }

    public List<String> getSkills(){
        return getList(SKILLS);
    }
    public void setSkills(List<String> skills){
        put(SKILLS, skills);
    }

    public void setDegree(Degree degree){
        put(DEGREE, degree);
    }
    public Degree getDegree(){
        return (Degree)getParseObject(DEGREE);
    }

    /**
    public ParseFile getCV(){
        return getParseFile(CV);
    }
    public void setCV(ParseFile cv){
        put(CV, cv);
    }
     **/

    public List<JSONObject> getEducation(){
        return getList(EDUCATION);
    }
    public void setEducation(List<JSONObject> education){
        put(EDUCATION, education);
    }

    public List<JSONObject> getExperience(){
        return getList(EXPERIENCE);
    }
    public void setExperience(List<JSONObject> experience){
        put(EXPERIENCE, experience);
    }


    public List<Offer> getFavouriteOffers(){
        return getList(FAVOURITE_OFFERS);
    }
    public void setFavouriteOffers(List<Offer> favouriteOffers){
        put(FAVOURITE_OFFERS, favouriteOffers);
    }

    public List<Candidature> getCandidatures(){
        return getList(CANDIDATURES);
    }
    public void setCandidatures(List<Candidature> candidatures){
        put(CANDIDATURES, candidatures);
    }


    public List<Company> getFavouriteCompanies(){
        return getList(FAVOURITE_COMPANIES);
    }
    public void setFavouriteCompanies(List<Company> favouriteCompanies){
        put(FAVOURITE_COMPANIES, favouriteCompanies);
    }

    /**
     * This method is used to add a new skill to the skills list of a student
     *
     * @param skill The skill to be added
     */
    public void addSkill(String skill){
        addUnique(SKILLS, skill);
    }

    /**
     * This method is used to remove one skill from the skills list of a student
     *
     * @param skill The skill to be removed
     */
    public void removeSkill(String skill){

        removeAll(SKILLS, Arrays.asList(skill));
    }

    /**
     * This method is used to add a new education entry to the education list of a student
     *
     * @param education The education entry to be added
     */
    public void addEducation(JSONObject education){
        addUnique(EDUCATION, education);
    }

    /**
     * This method is used to remove one education entry from the education list of a student
     *
     * @param education The education to be removed
     */
    public void removeEducation(JSONObject education){

        removeAll(EDUCATION, Arrays.asList(education));
    }

    /**
     * This method is used to add a new experience entry to the experience list of a student
     *
     * @param experience The experience entry to be added
     */
    public void addExperience(JSONObject experience){
        addUnique(EXPERIENCE, experience);
    }

    /**
     * This method is used to remove one experience entry from the experience list of a student
     *
     * @param experience The experience to be removed
     */
    public void removeExperience(JSONObject experience){

        removeAll(EXPERIENCE, Arrays.asList(experience));
    }


    public void addFavouriteOffer(Offer offer){
        addUnique(FAVOURITE_OFFERS, offer);
    }

    public void removeFavouriteOffer(Offer offer){

        removeAll(FAVOURITE_OFFERS, Arrays.asList(offer));
    }


    /**
     * This method is used to add a new offer to to the candidate offers list of a student
     *
     * @param candidature The candidature to be added
     */
    public void addCandidature(Candidature candidature){
        addUnique(CANDIDATURES, candidature);
    }

    /**
     * This method is used to remove one offer from the candidate offers list of a student
     *
     * @param candidature The candidature to be removed
     */
    public void removeCandidature(Candidature candidature){

        removeAll(CANDIDATURES, Arrays.asList(candidature));
    }


    public void addFavouriteCompany(Company company){
        addUnique(FAVOURITE_COMPANIES, company);
    }

    public void removeFavouriteCompany(Company company){
        removeAll(FAVOURITE_COMPANIES, Arrays.asList(company));
    }

    public void setHobbies(List<String> hobbies){
        put(HOBBIES, hobbies);
    }
    public List<String> getHobbies(){
        return getList(HOBBIES);
    }
    public void addHobby(String hobby){
        addUnique(HOBBIES, hobby);
    }
    public void removeHobby(String hobby){
        removeAll(HOBBIES, Arrays.asList(hobby));
    }



    public static JSONObject createJSONEduExp(String title, String description,
                                                 int startYear, int endYear) throws JSONException{

        JSONObject education = new JSONObject();

        education.put(EDU_EXP_TITLE, title);
        education.put(EDU_EXP_DESC, description);
        education.put(EDU_EXP_START_YEAR, startYear);
        education.put(EDU_EXP_END_YEAR, endYear);

        return education;
    }

    public static JSONObject createJSONEduExp(String title, String description,
                                              int startYear) throws JSONException{

        JSONObject education = new JSONObject();

        education.put(EDU_EXP_TITLE, title);
        education.put(EDU_EXP_DESC, description);
        education.put(EDU_EXP_START_YEAR, startYear);

        return education;
    }

    public String getKeywords() {
        return getString(KEYWORDS);
    }
    public void setKeywords(String keywords){
        put(KEYWORDS, keywords);
    }

    public void setNotices(List<Notice> notices){
        put(NOTICES, notices);
    }

    public List<Notice> getNotices(){
        return getList(NOTICES);
    }

    public void addNotice(Notice notice){
        addUnique(NOTICES, notice);
    }

    public void removeNotice(Notice notice){
        removeAll(NOTICES, Arrays.asList(notice));
    }

    public void setCourses(List<Course> courses){
        put(COURSES, courses);
    }
    public List<Course> getCourses(){
        return getList(COURSES);
    }
    public void addCourse(Course course){
        addUnique(COURSES, course);
    }
    public void removeCourse(Course course){
        removeAll(COURSES, Arrays.asList(course));
    }

    public void setShortDesc(String shortDesc){
        put(SHORT_DESC, shortDesc);
    }
    public String getShortDesc(){
        return getString(SHORT_DESC);
    }

    public void setLanguages(List<String> languages){
        put(LANGUAGES, languages);
    }
    public List<String> getLanguages(){
        return getList(LANGUAGES);
    }
    public void addLanguage(String language){
        addUnique(LANGUAGES, language);
    }
    public void removeLanguage(String language){
        removeAll(LANGUAGES, Arrays.asList(language));
    }

    public String createKeywordString(){
        int i;
        StringBuffer sb=new StringBuffer();

        if(this.getName()!=null) {
            sb.append(this.getName());
            sb.append("-");
        }

        if(this.getSurname()!= null) {
            sb.append(this.getSurname());
            sb.append("-");
        }

        if(this.getHobbies()!=null) {
            List<String> hobbies = new ArrayList<>(this.getHobbies());
            for (i = 0; i < hobbies.size(); i++) {
                sb.append(hobbies.get(i));
                sb.append("-");
            }
        }

        if(this.getLanguages()!=null) {
        List<String> languages=new ArrayList<>(this.getLanguages());
        for(i=0;i<languages.size();i++) {
            sb.append(languages.get(i));
            sb.append("-");
            }
        }

        if(this.getSkills() != null) {
            List<String> skills = new ArrayList<>(this.getSkills());
            for (i = 0; i < skills.size(); i++) {
                sb.append(skills.get(i));
                sb.append("-");
            }
        }

        sb.append(this.getDegree().getTitle());
        sb.append("-");

        sb.append(this.getDegree().getLevel());

        String s= sb.toString().toLowerCase().replaceAll(" ","");
        Log.d("Student Model Class","----->>>> "+s);
        return s;
    }
}

