package com.example.mad.campuspolito.student;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Student;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CreateGroupActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String USERS_LIST = "USERS_LIST";
    public static final int ACTION_RESULT_GROUP_CREATED = 3;
    public static final String NEW_CONVERSATION_ID = "NEW_CONVERSATION_ID";
    private final String TAG = CreateGroupActivity.class.getName();
    EditText mGroupNameView;
    EditText mGroupDescriptionView;
    ListView mStudentsListView;
    FloatingActionButton mAddGroupButton;
    LinearLayout mContent;
    LinearLayout mProgress;
    ArrayAdapter<AppUser> usersAdapter;


    private ArrayList<String> checkedUsersList;
    private String groupName;
    private String groupDescription;

    private CreateGroupConversation task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState != null  && savedInstanceState.containsKey(USERS_LIST))
            checkedUsersList = savedInstanceState.getStringArrayList(USERS_LIST);
        else
            checkedUsersList = null;

        mGroupNameView = (EditText)findViewById(R.id.group_name);
        mGroupDescriptionView = (EditText)findViewById(R.id.group_description);
        mStudentsListView = (ListView) findViewById(R.id.students_multiple_choice_list);
        mAddGroupButton = (FloatingActionButton) findViewById(R.id.add_group_button);

        mAddGroupButton.setOnClickListener(this);

        mContent = (LinearLayout)findViewById(R.id.content);
        mProgress = (LinearLayout)findViewById(R.id.create_group_progress);

        mProgress.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);
        mAddGroupButton.setVisibility(View.GONE);

        final Context ctx = this;

        ParseQuery<Student> studentParseQuery = ParseQuery.getQuery(Student.class);
        studentParseQuery.whereEqualTo(Student.IS_PUBLIC,false);
        studentParseQuery.findInBackground(new FindCallback<Student>() {
            @Override
            public void done(List<Student> list, ParseException e) {
                if (e != null) {
                    Snackbar.make(mAddGroupButton, R.string.parse_generic_error, Snackbar.LENGTH_LONG);
                    Log.e(TAG, "Error while obtaining students from Parse", e);
                } else {

                    ParseQuery<AppUser> query = ParseQuery.getQuery(AppUser.class);
                    query.whereEqualTo(AppUser.TYPE, AppUser.PROFILE_STUDENT);
                    query.whereNotEqualTo("objectId", AppUser.getCurrentUser().getObjectId());
                    query.whereNotContainedIn(AppUser.PROFILE_STUDENT,list);
                    query.findInBackground(new FindCallback<AppUser>() {
                        @Override
                        public void done(List<AppUser> list, ParseException e) {
                            mProgress.setVisibility(View.GONE);
                            mContent.setVisibility(View.VISIBLE);
                            mAddGroupButton.setVisibility(View.VISIBLE);

                            if(e != null){
                                Snackbar.make(mAddGroupButton,R.string.parse_generic_error,Snackbar.LENGTH_LONG);
                                Log.e(TAG, "Error while obtaining app users from Parse.", e);
                            }
                            else {
                                usersAdapter = new ArrayAdapter<AppUser>
                                        (ctx, android.R.layout.simple_list_item_multiple_choice, list);

                                mStudentsListView.setAdapter(usersAdapter);
                                mStudentsListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

                                if(checkedUsersList != null){
                                    AppUser user = null;
                                    for(String userId : checkedUsersList){ //for each checked user
                                        for(AppUser u : list){ //check in the list
                                            if(u.getObjectId().equals(userId)){
                                                user = u; //find the user object
                                                break;
                                            }
                                        }
                                        mStudentsListView.setItemChecked(usersAdapter.getPosition(user),true);
                                    }
                                }
                            }
                        }
                    });
                }

            }
        });






    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putStringArrayList(USERS_LIST, getCheckedUsersIds());
    }

    private ArrayList<String> getCheckedUsersIds(){
        SparseBooleanArray checked = mStudentsListView.getCheckedItemPositions();
        ArrayList<String> selectedItems = new ArrayList<String>();
        for (int i = 0; i < checked.size(); i++) {
            int position = checked.keyAt(i);
            if (checked.valueAt(i))
                selectedItems.add(usersAdapter.getItem(position).getObjectId());
        }

        return selectedItems;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        List<AppUser> checkedUsers = new ArrayList<AppUser>();

        SparseBooleanArray checked = mStudentsListView.getCheckedItemPositions();

        for (int i = 0; i < checked.size(); i++) {
            int position = checked.keyAt(i);
            if (checked.valueAt(i))
                checkedUsers.add(usersAdapter.getItem(position));
        }

        groupName = mGroupNameView.getText().toString().trim();
        groupDescription = mGroupDescriptionView.getText().toString().trim();

        Log.d(TAG, "Users checked are " + checkedUsers.size() + " for group " + groupName + " with " +
                "description " + groupDescription);


        if(TextUtils.isEmpty(groupName)){
            mGroupNameView.setError(getString(R.string.error_field_required));
            mGroupNameView.requestFocus();
            return;
        }

        if(checkedUsers.isEmpty() || checkedUsers.size() < 2){
            Snackbar.make(mAddGroupButton,R.string.new_group_no_users,Snackbar.LENGTH_SHORT).show();;
            mStudentsListView.requestFocus();
            return;

        }

        task = new CreateGroupConversation(this,checkedUsers, groupName, groupDescription);

        task.execute();

    }


    private class CreateGroupConversation extends AsyncTask<Void,Void,Boolean> {

        final Context context;
        private final List<AppUser> users;
        private final String name;
        private final String description;

        private String conversationId;


        public CreateGroupConversation(Context context, List<AppUser> users,
                                       String name, String description){
            this.context = context;
            this.users = users;
            this.name = name;
            this.description = description;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mContent.setVisibility(View.GONE);
            mAddGroupButton.hide();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            try{
                Conversation c = new Conversation();
                c.setGroupName(name);
                c.setGroupDescription(description);

                c.setUsers(users);
                c.addUser((AppUser) AppUser.getCurrentUser());
                c.setGroupAdministrator((AppUser)AppUser.getCurrentUser());

                c.save();

                conversationId = c.getObjectId();
                return true;
            }
            catch (ParseException e ){
                Log.e(TAG, "Error while creating conversation",e);
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if(success){
                Intent data = new Intent();
                data.putExtra(NEW_CONVERSATION_ID,conversationId);
                setResult(ACTION_RESULT_GROUP_CREATED, data);
                finish();
            }
            else {
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
                mAddGroupButton.show();
                Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
                Log.d(TAG, "Save of data was unsuccessful");
            }
        }

        @Override
        protected void onCancelled() {
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
            mAddGroupButton.show();
            Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
            Log.d(TAG, "Saving of data was cancelled");
        }
    }
}
