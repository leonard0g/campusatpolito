package com.example.mad.campuspolito.teacher;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.HomeActivity;
import com.example.mad.campuspolito.common.LectureDetailActivity;
import com.example.mad.campuspolito.common.ShowCourseDetailsActivity;
import com.example.mad.campuspolito.common.TimetableListActivity;
import com.example.mad.campuspolito.locations.LocationSearchMasterActivity;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Teacher;
import com.example.mad.campuspolito.student.ConversationListActivity;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TeacherHomeActivity extends HomeActivity {

    private TextView mHomeName;
    private TextView mHomeDescription;
    private TextView mTeacherDepartment;
    private TextView mTeacherRole;
    private CircularImageView mHomeImage;
    private LinearLayout mProgress;
    private NestedScrollView mContent;
    private LinearLayout mTodayLessons;
    private TextView mToday;

    private Teacher profile;
    private FetchHomeData task;

    @Override
    protected void onSetHomeContentView() {
        setContentView(R.layout.activity_teacher_home);

        mHomeName = (TextView)findViewById(R.id.home_name);
        mHomeDescription = (TextView)findViewById(R.id.home_description);
        mTeacherDepartment = (TextView)findViewById(R.id.teacher_department_id);
        mTeacherRole = (TextView)findViewById(R.id.teacher_role);
        mHomeImage = (CircularImageView)findViewById(R.id.home_image);
        mProgress = (LinearLayout)findViewById(R.id.home_progress);
        mContent = (NestedScrollView)findViewById(R.id.home_content);
        mTodayLessons = (LinearLayout)findViewById(R.id.today_lessons);
        mToday = (TextView)findViewById(R.id.today);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(false);
    }

    public class FetchHomeData extends AsyncTask<Void,Void,Boolean> {

        private final Context ctx;
        List<Conversation> latestConversations;
        List<Lecture> todayLectures;

        public FetchHomeData(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                profile = currentUser.getTeacherProfile();
                profile.fetchIfNeeded();

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Basic info about teacher loaded.");
                    return false;
                }

                Log.d(TAG, "Basic info about teacher loaded.");

                if(profile.getCourses() != null){

                    ParseQuery<Lecture> lessonsQuery = ParseQuery.getQuery(Lecture.class);

                    Calendar c = Calendar.getInstance();
                    lessonsQuery.whereEqualTo(Lecture.DAY, c.get(Calendar.DAY_OF_MONTH));
                    lessonsQuery.whereEqualTo(Lecture.MONTH, c.get(Calendar.MONTH));
                    lessonsQuery.whereEqualTo(Lecture.YEAR, c.get(Calendar.YEAR));
                    lessonsQuery.whereContainedIn(Lecture.COURSE, profile.getCourses());
                    lessonsQuery.orderByAscending(Lecture.START_SLOT);
                    lessonsQuery.include(Lecture.COURSE);
                    lessonsQuery.include(Lecture.ROOM);

                    todayLectures = new ArrayList<Lecture>(lessonsQuery.find());

                }
                else
                    todayLectures = new ArrayList<>();

                Log.d(TAG, "Info about lectures loaded.");

                if(isCancelled()){
                    Log.e(TAG, "Async task cancelled. Basic info about lectures has been loaded.");
                    return false;
                }

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching home data.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){

                mToday.setText(Calendar.getInstance().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) +
                        " " + android.text.format.DateFormat.getDateFormat(ctx).format(Calendar.getInstance().getTime()));
                mHomeName.setText(currentUser.toString());
                mHomeDescription.setText(profile.getIdNumber());
                mTeacherDepartment.setText(profile.getDepartmentId());
                mTeacherRole.setText(profile.getRole());
                if(currentUser.getPicture() != null){
                    mHomeImage.setParseFile(currentUser.getPicture());
                    mHomeImage.loadInBackground();
                }

                if(todayLectures.size() == 0){
                    mTodayLessons.removeAllViews();
                    findViewById(R.id.no_today_lessons).setVisibility(View.VISIBLE);
                }
                else {
                    findViewById(R.id.no_today_lessons).setVisibility(View.GONE);
                    DynamicLinearLayoutViewInflater.addLectureViewsToLayout(todayLectures,
                            ctx, mTodayLessons, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String lectureId = (String) v.getTag(R.id.today_lessons);
                                    if (lectureId != null) {
                                        Intent showLectureDetails = new Intent(ctx, LectureDetailActivity.class);
                                        showLectureDetails.putExtra(LectureDetailActivity.LECTURE_ID, lectureId);
                                        startActivity(showLectureDetails);
                                    }

                                }
                            }, R.id.today_lessons);
                }
            }
            else {
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
                Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_LONG).show();
            }

            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
        }

        @Override
        protected void onCancelled() {
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            Snackbar.make(mContent,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onSubscribeToPushChannels() {

        try {
            if(profile == null)
                profile = currentUser.getTeacherProfile();

            profile.fetchIfNeeded();

            List<Course> courseList = profile.getCourses();

            if(courseList != null && courseList.size()> 0){

                ParseObject.fetchAllIfNeeded(courseList);

                Log.d(TAG, "Current installation Id is :" + ParseInstallation.getCurrentInstallation().getInstallationId());
                Log.d(TAG, "Current channels are: " + ParseInstallation.getCurrentInstallation().getList("channels"));
                for(Course course : courseList){
                    if(!ParseInstallation.getCurrentInstallation().getList("channels").contains(CampusPolitoApplication.COURSE_CHANNEL+course.getObjectId())){
                        Log.d(TAG, "Subscribing to the channel " + CampusPolitoApplication.COURSE_CHANNEL + course.getObjectId());
                        ParsePush.subscribeInBackground(CampusPolitoApplication.COURSE_CHANNEL + course.getObjectId(), new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e != null) {
                                    Log.e(TAG, "Error while subscribing in background to course channel.", e);
                                } else
                                    Log.d(TAG, "Course channel subscribed.");
                            }
                        }); //Subscribe to push notifications for conversation
                    }
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean OnDrawerMenuItemSelected(int itemId) {
        switch (itemId) {
            case R.id.teacher_courses:
                Intent coursesIntent = new Intent(this,TeacherCoursesListActivity.class);
                startActivity(coursesIntent);
                return true;
            case R.id.teacher_timetable:
                Intent timetableIntent = new Intent(this,TimetableListActivity.class);
                startActivity(timetableIntent);
                return true;
            case R.id.teacher_search_locations:
                Intent locationsIntent = new Intent(this, LocationSearchMasterActivity.class);
                startActivity(locationsIntent);
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void onInflateHomeMenu(Menu menu) {

    }

    @Override
    protected boolean onHomeMenuItemSelected(int id) {
        return false;
    }

    @Override
    protected void onHomeViewClick(View v) {

    }

    @Override
    protected void onResumeHomeActivity() {
        task = new FetchHomeData(this);
        task.execute();

    }
}
