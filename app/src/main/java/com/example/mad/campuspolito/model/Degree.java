package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by mdb on 24/07/15.
 */
@ParseClassName("Degree")
public class Degree extends ParseObject{

    public static final String TITLE = "title";
    public static final String LEVEL = "level";
    public static final String COURSES = "courses";

    public Degree(){

    }

    public void setTitle(String title){
        put(TITLE, title);
    }

    public String getTitle(){
        return getString(TITLE);
    }

    public void setLevel(String level) {
        put(LEVEL, level);
    }
    public String getLevel(){
        return getString(LEVEL);
    }

    public void setCourses(List<Course> courses){
        put(COURSES, courses);
    }
    public List<Course> getCourses(){
        return getList(COURSES);
    }
}
