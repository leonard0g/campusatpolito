package com.example.mad.campuspolito.student;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Message;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;

import java.text.DateFormat;

/**
 * Created by mdb on 23/07/15.
 */
public class MessagesAdapter extends ParseQueryAdapter<Message> {

    private static final int MSG_CURRENT_USER = 0;
    private static final int MSG_OTHER_USER = 1;
    private Context ctx;
    private DateFormat dateFormat;
    private DateFormat timeFormat;

    private boolean isGroupChat;
    private String currentUserId;

    private Message m;

    public MessagesAdapter(Context context, com.parse.ParseQueryAdapter.QueryFactory<Message> queryFactory,boolean isGroupChat){
        super(context, queryFactory);
        this.ctx = context;
        this.dateFormat = android.text.format.DateFormat.getDateFormat(context);
        this.timeFormat = android.text.format.DateFormat.getTimeFormat(context);
        this.isGroupChat = isGroupChat;
        this.currentUserId = AppUser.getCurrentUser().getObjectId();
    }

    @Override
    public Message getItem(int index) {
        return super.getItem(super.getCount() - index - 1);
    }

    @Override
    public View getItemView(Message object, View v, ViewGroup parent) {

        MsgCurrentUserHolder msgCurrentUserHolder = null;
        MsgOtherUserHolder msgOtherUserHolder = null;

        int type;


        if(object == null){
            return new Space(getContext());
        }
        if(object.getSender().getObjectId().equals(currentUserId))
            type = MSG_CURRENT_USER;
        else
            type = MSG_OTHER_USER;

        if(type == MSG_CURRENT_USER){
            if(v == null || ! (v.getTag() instanceof  MsgCurrentUserHolder)){
                v = View.inflate(getContext(), R.layout.msg_row_current, null);
                msgCurrentUserHolder = new MsgCurrentUserHolder();
                msgCurrentUserHolder.msgData = (TextView)v.findViewById(R.id.msg_data);
                msgCurrentUserHolder.msgDate = (TextView)v.findViewById(R.id.msg_date);
                msgCurrentUserHolder.msgSender = (TextView)v.findViewById(R.id.msg_sender);

                v.setTag(msgCurrentUserHolder);
            }
            else
                msgCurrentUserHolder = (MsgCurrentUserHolder)v.getTag();


            msgCurrentUserHolder.msgData.setText(object.getData());
            msgCurrentUserHolder.msgDate.setText(timeFormat.format(object.getCreatedAt()) + " " + dateFormat.format(object.getCreatedAt()));
            if(isGroupChat){
                if(msgCurrentUserHolder.msgSender.getVisibility() != View.VISIBLE)
                    msgCurrentUserHolder.msgSender.setVisibility(View.VISIBLE);

                msgCurrentUserHolder.msgSender.setText(object.getSender().toString());
            }

            else
                msgCurrentUserHolder.msgSender.setVisibility(View.GONE);
        }
        else {
            if(v == null || !(v.getTag() instanceof MsgOtherUserHolder)){
                v = View.inflate(getContext(), R.layout.msg_row_other, null);
                msgOtherUserHolder = new MsgOtherUserHolder();
                msgOtherUserHolder.msgData = (TextView)v.findViewById(R.id.msg_data);
                msgOtherUserHolder.msgDate = (TextView)v.findViewById(R.id.msg_date);
                msgOtherUserHolder.msgSender = (TextView)v.findViewById(R.id.msg_sender);

                v.setTag(msgOtherUserHolder);
            }
            else
                msgOtherUserHolder = (MsgOtherUserHolder)v.getTag();

            msgOtherUserHolder.msgData.setText(object.getData());
            msgOtherUserHolder.msgDate.setText(timeFormat.format(object.getCreatedAt()) + " " + dateFormat.format(object.getCreatedAt()));
            if(isGroupChat){
                if(msgOtherUserHolder.msgSender.getVisibility() != View.VISIBLE)
                    msgOtherUserHolder.msgSender.setVisibility(View.VISIBLE);

                AppUser sender = object.getSender();
                try {
                    sender.fetchIfNeeded();
                    msgOtherUserHolder.msgSender.setText(sender.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            else
                msgOtherUserHolder.msgSender.setVisibility(View.GONE);
        }


        // Take advantage of ParseQueryAdapter's getItemView logic for
        // populating the main TextView/ImageView.
        // The IDs in your custom layout must match what ParseQueryAdapter expects
        // if it will be populating a TextView or ImageView for you.

        return v;
    }

    private class MsgCurrentUserHolder {
        public TextView msgData;
        public TextView msgDate;
        public TextView msgSender;
    }

    private class MsgOtherUserHolder {
        public TextView msgData;
        public TextView msgDate;
        public TextView msgSender;
    }
}
