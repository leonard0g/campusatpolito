package com.example.mad.campuspolito.student;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Conversation;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A list fragment representing a list of Conversations. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link ConversationDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class ConversationListFragment extends ListFragment implements ParseQueryAdapter.OnQueryLoadListener<Conversation>{

    private static final String TAG = ConversationListFragment.class.getName();
    private LinearLayout mProgressBar;
    private FrameLayout mContent;
    private List<Conversation> conversationList;
    private ConversationsAdapter adapter;

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    @Override
    public void onLoading() {
        Log.d(TAG, "Loading conversations...");
        mProgressBar.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);
    }

    @Override
    public void onLoaded(List<Conversation> list, Exception e) {
        if(list != null)
            Log.d(TAG, "Conversations loaded...(number "+list.size()+" )");
        else
            Log.d(TAG, "Conversations loaded...empty list");
        mProgressBar.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);

        if(e == null)
            conversationList = list;
        else{
            Log.e(TAG, "An error occurred while loading conversations list from Parse service...",e);
            Snackbar.make(mContent,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();
        }

    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onConversationSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onConversationSelected(String id) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ConversationListFragment() {
    }

    public void onUpdateContent() {
        if(adapter != null)
            adapter.loadObjects();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.conversations_list,container,false);

        mProgressBar = (LinearLayout)v.findViewById(R.id.list_progress);
        mContent = (FrameLayout)v.findViewById(R.id.content);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        final AppUser currentUser = (AppUser)AppUser.getCurrentUser();

         adapter = new ConversationsAdapter(getActivity(), new ParseQueryAdapter.QueryFactory<Conversation>() {
            @Override
            public ParseQuery<Conversation> create() {
                ParseQuery<Conversation> query1to1Conv = ParseQuery.getQuery(Conversation.class);
                query1to1Conv.whereExists(Conversation.LAST_MSG);
                query1to1Conv.whereDoesNotExist(Conversation.OPT_GRP_ADMINISTRATOR);

                ParseQuery<Conversation> queryGroupConv = ParseQuery.getQuery(Conversation.class);
                queryGroupConv.whereExists(Conversation.OPT_GRP_ADMINISTRATOR);

                ParseQuery<Conversation> query = ParseQuery.or(Arrays.asList(query1to1Conv, queryGroupConv));

                query.whereContainsAll(Conversation.USERS, Arrays.asList(currentUser));
                if(currentUser.getHiddenConversations() != null && currentUser.getHiddenConversations().size() > 0)
                    query.whereNotContainedIn("objectId",currentUser.getHiddenConversations());
                query.orderByDescending("updatedAt");

                return query;
            }
        });

        adapter.setTextKey(Conversation.LAST_MSG);
        adapter.addOnQueryLoadListener(this);

        setListAdapter(adapter);

        if(getActivity() instanceof ConversationListActivity)
            if(((ConversationListActivity)getActivity()).mTwoPane)
                setActivateOnItemClick(true);

        Log.d(TAG, "List Fragment created and adapter set.");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onConversationSelected(conversationList.get(position).getObjectId());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
}
