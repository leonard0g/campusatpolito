package com.example.mad.campuspolito.common;


import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class FullscreenImageActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;


    public static final String INTENT_IMAGE_URL = "IMAGE_URL";
    private ImageView imageView;
    private String imageUrl;
    private TextView textView;
    private ProgressBar progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen_image);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);


        if(savedInstanceState != null){
            imageUrl = savedInstanceState.getString(INTENT_IMAGE_URL);
        }
        else{
            imageUrl = getIntent().getStringExtra(INTENT_IMAGE_URL);
        }

        imageView = (ImageView)findViewById(R.id.image);
        textView = (TextView)findViewById(R.id.text);
        progressView = (ProgressBar)findViewById(R.id.progress);

        InputStream bitmapStream = null;
        try {
            if(imageUrl.contains("http")){

                DownloadTask task = new DownloadTask(imageUrl,imageView);
                task.execute();
            }
            else{
                bitmapStream = getContentResolver().openInputStream(Uri.parse(imageUrl));
                Bitmap bitmapObject = BitmapFactory.decodeStream(bitmapStream);
                imageView.setImageBitmap(bitmapObject);
            }

        }
        catch(SecurityException e){
            e.printStackTrace();
            imageView.setImageBitmap(null);
        }
        catch (FileNotFoundException e) {
            imageView.setImageBitmap(null);
        }

        finally {
            try{
                if(bitmapStream != null)
                    bitmapStream.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, imageView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!CampusPolitoApplication.checkConnectivity(getApplicationContext()))
            Toast.makeText(getApplicationContext(), getString(R.string.no_connectivity), Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }


    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private class DownloadTask extends AsyncTask<Void, Void, Boolean> {

        String URL;

        InputStream stream;
        Bitmap bitmap;
        ImageView imageView;

        public DownloadTask(String URL, ImageView imageView){
            this.URL = URL;
            this.imageView = imageView;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                java.net.URL url = new URL(URL);

                stream = url.openConnection().getInputStream();

                bitmap = BitmapFactory.decodeStream(stream);

            } catch (MalformedURLException e) {
                return false;
            } catch (IOException e) {
                return false;
            }
            finally {
                try {
                    if(stream!= null)
                        stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean completed) {

            textView.setVisibility(View.GONE);
            progressView.setVisibility(View.GONE);

            if(completed){
                imageView.setImageBitmap(bitmap);
            }
            else {
                imageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_picture_load));
            }

        }
    }
}
