package com.example.mad.campuspolito.common;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppGlobals;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.model.Teacher;
import com.example.mad.campuspolito.utils.DatePickerFragment;
import com.example.mad.campuspolito.utils.ParseApplicationGlobalsQuery;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import bolts.Task;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * A sign up screen for app users.
 */
public class SignupActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, DatePickerFragment.DatePickerInteraction {

    private static final String TAG = SignupActivity.class.getName();

    private static final String STUDENT_DEGREE_TITLES = "student_degree_titles";
    private static final String STUDENT_DEGREE_LEVELS = "student_degree_levels";
    private static final String DATE_PICKER = "DATE_PICKER";

    private ArrayList<String> studentDegreeTitles;
    private ArrayList<String> studentDegreeLevels;
    /**
     * Keep track of the signup task to ensure we can cancel it if requested.
     */

    private static final int RESULT_LOAD_IMG = 0;

    private UserSignupTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mNameView;
    private EditText mSurnameView;
    private TextView mDateOfBirthView;
    private EditText mPhone;
    private EditText mPasswordView;
    private EditText mRepeatPasswordView;
    private EditText mVATNumber;
    private EditText mIdNumber;
    private MaterialSpinner mDegreeTitleView;
    private MaterialSpinner mDegreeLevelView;
    private View mProgressView;
    private View mSignupFormView;
    private FloatingActionButton mEmailSignUpButton;
    private TabLayout.Tab studentSignupTab;
    private TabLayout.Tab teacherSignupTab;
    private TabLayout.Tab companySignupTab;
    private ImageView pictureView;
    private Uri pictureUri;
    private Bitmap pictureBitmap;


    private DatePickerFragment datePickerFragment;

    //variables for date picker management
    protected int mYear;
    protected int mMonth;
    protected int mDay;
    DateFormat format;

    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pictureView = (ImageView)findViewById(R.id.picture);
        pictureUri = null;
        pictureBitmap = null;

        ctx = this;

        if(savedInstanceState != null && savedInstanceState.containsKey(STUDENT_DEGREE_TITLES)
                && savedInstanceState.containsKey(STUDENT_DEGREE_LEVELS)){
            studentDegreeTitles = savedInstanceState.getStringArrayList(STUDENT_DEGREE_TITLES);
            studentDegreeLevels = savedInstanceState.getStringArrayList(STUDENT_DEGREE_LEVELS);
        }
        else{
            studentDegreeTitles = null;
            studentDegreeLevels = null;
        }

        mSignupFormView = findViewById(R.id.signup_form);
        mProgressView = findViewById(R.id.signup_progress);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        studentSignupTab = tabLayout.newTab().setText(getString(R.string.student));
        teacherSignupTab = tabLayout.newTab().setText(getString(R.string.teacher));
        companySignupTab = tabLayout.newTab().setText(getString(R.string.company));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        findViewById(R.id.name).setVisibility(View.VISIBLE);
                        findViewById(R.id.id_number).setVisibility(View.VISIBLE);
                        findViewById(R.id.surname).setVisibility(View.VISIBLE);
                        findViewById(R.id.vatnumber).setVisibility(View.GONE);
                        findViewById(R.id.birthday).setVisibility(View.VISIBLE);
                        findViewById(R.id.student_degree_title).setVisibility(View.VISIBLE);
                        findViewById(R.id.student_degree_level).setVisibility(View.VISIBLE);
                        findViewById(R.id.phone).setVisibility(View.VISIBLE);
                        mDateOfBirthView = (TextView) findViewById(R.id.birthday);
                        mDateOfBirthView.setFocusable(false);
                        mDateOfBirthView.setFocusableInTouchMode(true);
                        mDateOfBirthView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if (b){
                                    datePickerFragment = new DatePickerFragment();
                                    datePickerFragment.show(getSupportFragmentManager(), DATE_PICKER);
                                }
                            }
                        });
                        mDateOfBirthView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                datePickerFragment = new DatePickerFragment();
                                datePickerFragment.show(getSupportFragmentManager(), DATE_PICKER);
                            }
                        });


                        mDegreeTitleView = (MaterialSpinner) findViewById(R.id.student_degree_title);
                        mDegreeLevelView = (MaterialSpinner) findViewById(R.id.student_degree_level);

                        if (studentDegreeTitles == null && studentDegreeLevels == null) {
                            LoadDegreeTitlesAndLevels task = new LoadDegreeTitlesAndLevels(ctx);
                            task.execute();
                        } else {
                            mDegreeTitleView.setAdapter(new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, studentDegreeTitles));
                            mDegreeLevelView.setAdapter(new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, studentDegreeLevels));
                        }

                        break;
                    case 1:

                        Snackbar.make(mSignupFormView, getString(R.string.teacher_signup_notice), Snackbar.LENGTH_SHORT).show();
                        findViewById(R.id.id_number).setVisibility(View.VISIBLE);
                        findViewById(R.id.name).setVisibility(View.GONE);
                        findViewById(R.id.surname).setVisibility(View.GONE);
                        findViewById(R.id.vatnumber).setVisibility(View.GONE);
                        findViewById(R.id.birthday).setVisibility(View.GONE);
                        findViewById(R.id.student_degree_title).setVisibility(View.GONE);
                        findViewById(R.id.student_degree_level).setVisibility(View.GONE);
                        findViewById(R.id.phone).setVisibility(View.GONE);
                        break;
                    case 2:
                        findViewById(R.id.name).setVisibility(View.VISIBLE);
                        findViewById(R.id.id_number).setVisibility(View.GONE);
                        findViewById(R.id.surname).setVisibility(View.GONE);
                        findViewById(R.id.vatnumber).setVisibility(View.VISIBLE);
                        findViewById(R.id.birthday).setVisibility(View.GONE);
                        findViewById(R.id.student_degree_title).setVisibility(View.GONE);
                        findViewById(R.id.student_degree_level).setVisibility(View.GONE);
                        findViewById(R.id.phone).setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.addTab(studentSignupTab);
        tabLayout.addTab(teacherSignupTab);
        tabLayout.addTab(companySignupTab);
        studentSignupTab.select();

        format = android.text.format.DateFormat.getDateFormat(getApplicationContext());

        // Set up the signup form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();


        mIdNumber = (EditText)findViewById(R.id.id_number);
        mNameView = (EditText) findViewById(R.id.name);
        mRepeatPasswordView = (EditText) findViewById(R.id.repeatPassword);
        mPhone = (EditText) findViewById(R.id.phone);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        mPasswordView = (EditText) findViewById(R.id.password);

        mEmailSignUpButton = (FloatingActionButton) findViewById(R.id.email_sign_up_button);
        mEmailSignUpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignup();
            }
        });

        pictureView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPicture();
            }
        });
        mEmailView.setImeOptions(EditorInfo.IME_ACTION_NEXT);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(datePickerFragment != null)
            datePickerFragment.dismiss();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(studentDegreeTitles != null && studentDegreeLevels != null){
            outState.putStringArrayList(STUDENT_DEGREE_TITLES,studentDegreeTitles);
            outState.putStringArrayList(STUDENT_DEGREE_LEVELS,studentDegreeLevels);
        }
    }

    /** Utility method used to load an image as profile picture **/
    private void loadPicture() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Attempts to register the account specified by the signup form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual registration attempt is made.
     */
    public void attemptSignup() {
        hideSoftKeyboard(mEmailSignUpButton);
        if (mAuthTask != null) {
            return;
        }

        boolean isStudent = false;
        boolean isTeacher = false;
        boolean isCompany = false;

        if(studentSignupTab.isSelected())
            isStudent = true;
        else if (teacherSignupTab.isSelected())
            isTeacher = true;
        else if (companySignupTab.isSelected())
            isCompany = true;

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the signup attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String repeatPassword = mRepeatPasswordView.getText().toString();
        String name = mNameView.getText().toString();
        String telephone = mPhone.getText().toString();
        String surname = null;
        String vatnumber = null;
        String dateOfBirth = null;
        String degreeTitle = null;
        String degreeLevel = null;
        String idNumber = null;


        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if(!password.equals(repeatPassword)){
            mPasswordView.setError(getString(R.string.unmatching_passwords));
            focusView = mPasswordView;
            cancel = true;
        }

        if (!isTeacher && TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if(!isTeacher && TextUtils.isEmpty(telephone)){
            mPhone.setError(getString(R.string.error_field_required));
            focusView = mPhone;
            cancel = true;
        }



        mDateOfBirthView = (TextView) findViewById(R.id.birthday);

        if(isStudent){
            mDateOfBirthView = (TextView)findViewById(R.id.birthday);
            dateOfBirth = mDateOfBirthView.getText().toString();
            if (TextUtils.isEmpty(dateOfBirth)) {
                mDateOfBirthView.setError(getString(R.string.error_field_required));
                focusView = mDateOfBirthView;
                cancel = true;
            }

            mSurnameView = (EditText) findViewById(R.id.surname);
            surname = mSurnameView.getText().toString();
            if (TextUtils.isEmpty(surname)) {
                mSurnameView.setError(getString(R.string.error_field_required));
                focusView = mSurnameView;
                cancel = true;
            }

            degreeTitle = (String)mDegreeTitleView.getSelectedItem();
            if(TextUtils.isEmpty(degreeTitle) || degreeTitle.equals(getString(R.string.prompt_student_degree_title))){
                mDegreeTitleView.setError(getString(R.string.error_field_required));
                focusView = mDegreeTitleView;
                cancel = true;
            }

            degreeLevel = (String)mDegreeLevelView.getSelectedItem();
            if(TextUtils.isEmpty(degreeLevel) || degreeLevel.equals(getString(R.string.prompt_student_degree_level))){
                mDegreeLevelView.setError(getString(R.string.error_field_required));
                focusView = mDegreeLevelView;
                cancel = true;
            }

            idNumber = (String)mIdNumber.getText().toString().trim().toLowerCase();

            if(TextUtils.isEmpty(idNumber) || !idNumber.startsWith("s")){
                mIdNumber.setError(getString(R.string.error_student_id_number));
                focusView = mIdNumber;
                cancel = true;
            }
        }
        else if (isTeacher){


            idNumber = (String)mIdNumber.getText().toString().trim().toLowerCase();

            if(TextUtils.isEmpty(idNumber) || !idNumber.startsWith("d")){
                mIdNumber.setError(getString(R.string.error_teacher_id_number));
                focusView = mIdNumber;
                cancel = true;
            }
        }
        else {

            mVATNumber = (EditText) findViewById(R.id.vatnumber);
            vatnumber = mVATNumber.getText().toString();
            if(TextUtils.isEmpty(vatnumber)){
                mVATNumber.setError(getString(R.string.error_field_required));
                focusView = mVATNumber;
                cancel = true;
            }
        }

        if (cancel) {
            // There was an error; don't attempt signup and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user signup attempt.
            showProgress(true);
            mAuthTask = new UserSignupTask(this,email, password, name, surname,
                    dateOfBirth,telephone,vatnumber,isStudent, isTeacher, isCompany,pictureUri,degreeTitle, degreeLevel,idNumber);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > CampusPolitoApplication.MIN_PASSWORD_LENGTH;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!CampusPolitoApplication.checkConnectivity(getApplicationContext()))
            Toast.makeText(getApplicationContext(), getString(R.string.no_connectivity), Toast.LENGTH_LONG).show();
    }

    private void hideSoftKeyboard(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Shows the progress UI and hides the Signup form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSignupFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mEmailSignUpButton.setVisibility(show ? View.GONE : View.VISIBLE);
            mSignupFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSignupFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    mEmailSignUpButton.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSignupFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mEmailSignUpButton.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<String>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    @Override
    public Calendar setDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, mYear);
        c.set(Calendar.MONTH, mMonth);
        c.set(Calendar.DAY_OF_MONTH, mDay);
        return c;
    }

    @Override
    public void getDate(int year, int month, int day) {
        mYear = year;
        mMonth = month;
        mDay = day;
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, mYear);
        c.set(Calendar.MONTH, mMonth);
        c.set(Calendar.DAY_OF_MONTH, mDay);

        mDateOfBirthView.setText(format.format(c.getTime()));

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(SignupActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    protected void updateDisplay() {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, mDay);
        c.set(Calendar.MONTH,mMonth);
        c.set(Calendar.YEAR, mYear);

    }

    private class LoadDegreeTitlesAndLevels extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;

        public LoadDegreeTitlesAndLevels(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mSignupFormView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try{

                ParseQuery<Degree> query = ParseQuery.getQuery(Degree.class);
                List<Degree> degreeList = query.find();

                HashMap<String,Object> degreeTitles = new HashMap<String,Object>();
                HashMap<String,Object> degreeLevels = new HashMap<String,Object>();
                Object obj = new Object();

                for(Degree d : degreeList){
                    if(degreeTitles.get(d.getTitle()) == null)
                        degreeTitles.put(d.getTitle(),obj);

                    if(degreeLevels.get(d.getLevel()) == null)
                        degreeLevels.put(d.getLevel(),obj);
                }

                studentDegreeTitles = new ArrayList<>(degreeTitles.keySet());
                studentDegreeLevels = new ArrayList<>(degreeLevels.keySet());


                Collections.sort(studentDegreeTitles);

                if(!studentDegreeTitles.isEmpty() && !studentDegreeLevels.isEmpty())
                    return true;

                else{
                    Log.e(TAG, "Server error: degree titles or levels (or both) are empty.");
                    return false;
                }

            }
            catch (com.parse.ParseException e){
                Log.e(TAG, "Error while retrieving degree elements from Parse service.",e);
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean success) {

            mSignupFormView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.GONE);

            if(success){
                mDegreeTitleView.setAdapter(new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item,studentDegreeTitles));
                mDegreeLevelView.setAdapter(new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item,studentDegreeLevels));
            }
            else{
                Log.e(TAG, "Unable to retrieve degree titles and levels for students. Retry again or check Server status.");
                Snackbar.make(mSignupFormView,R.string.parse_generic_error,Snackbar.LENGTH_LONG);
            }

        }
    }


    /**
     * Represents an asynchronous Signup task used to authenticate
     * the user.
     */
    public class UserSignupTask extends AsyncTask<Void, Integer, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private final String mName;
        private final String mSurname;
        private Date mDateOfBirth;
        private boolean mIsStudent = false;
        private boolean mIsTeacher = false;
        private boolean mIsCompany = false;
        private String mPhone;
        private String mVatnumber;
        private Context ctx;
        private Uri mPictureUri;
        private String mDegreeTitle;
        private String mDegreeLevel;
        private String mIdNumber;

        ProgressDialog barProgressDialog;

        UserSignupTask(Context ctx, String email, String password, String name, String surname,
                       String dateOfBirth, String telephone, String vatNumber, boolean isStudent,
                       boolean isTeacher, boolean isCompany, Uri pictureUri, String degreeTitle, String degreeLevel,String idNumber) {

            this.ctx = ctx;
            mEmail = email;
            mPassword = password;
            mName = name;
            mSurname = surname;
            try {
                if(isStudent)
                    mDateOfBirth = android.text.format.DateFormat.getDateFormat(ctx).parse(dateOfBirth);
            } catch (ParseException e) {
                Log.e(TAG, "Date of birth is invalid format.", e);
            }

            mIsStudent = isStudent;
            mIsTeacher = isTeacher;
            mIsCompany = isCompany;
            mPhone = telephone;
            mVatnumber = vatNumber;

            mPictureUri = pictureUri;

            mDegreeTitle = degreeTitle;
            mDegreeLevel = degreeLevel;
            mIdNumber = idNumber;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            if(mPictureUri != null){
                barProgressDialog = new ProgressDialog(ctx);

                barProgressDialog.setTitle(getString(R.string.upload_image_title));
                barProgressDialog.setMessage(getString(R.string.upload_image_msg));
                barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                barProgressDialog.setProgress(0);
                barProgressDialog.setMax(100);
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Integer progress = values[0];
            if(!barProgressDialog.isShowing())
                barProgressDialog.show();

            barProgressDialog.setProgress(progress);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            AppUser user = new AppUser();

            if(!mIsTeacher){
                user.setName(mName);
                if(mSurname != null)
                    user.setSurname(mSurname);
                user.setPhone(mPhone);
            }

            user.setUsername(mEmail);
            user.setEmail(mEmail);

            user.setPassword(mPassword);

            Student studentProfile = null;
            Teacher teacherProfile = null;
            Company companyProfile = null;

            ByteArrayOutputStream outputStream = null;
            Bitmap bitmapObject;
            ParseFile image = null;
            try {

                if(mPictureUri != null){

                    //The file upload shall continue if a single file fails to be loaded
                    Log.d(TAG, "Processing picture");
                    bitmapObject = CampusPolitoApplication.decodeSampledBitmapFromUri(mPictureUri,
                            CampusPolitoApplication.SCALE_SIZE + 1,
                            CampusPolitoApplication.SCALE_SIZE + 1);


                    bitmapObject = CampusPolitoApplication.scaleBitmapAndKeepRatio(bitmapObject,
                            CampusPolitoApplication.SCALE_SIZE,
                            CampusPolitoApplication.SCALE_SIZE);


                    Log.d(TAG, "Bitmap object saved from content provider.");

                    outputStream = new ByteArrayOutputStream();
                    bitmapObject.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                    Log.d(TAG, "Bitmap object compressed to output stream (size " + outputStream.size() + ")");
                    image = new ParseFile("profile_pic.jpg", outputStream.toByteArray());

                    outputStream.close();
                    Log.d(TAG, "Image loaded to Parse File object, starting upload...");


                    Task<Void> saveTask = image.saveInBackground(new ProgressCallback() {
                        @Override
                        public void done(Integer integer) {
                            publishProgress(integer);
                        }
                    });

                    saveTask.waitForCompletion();


                    if (saveTask.isCompleted()) {
                        Log.d(TAG, "Picture saved to Parse.com");
                        user.setPicture(image);
                    }
                }
                if(mIsStudent){
                    user.setType(AppUser.PROFILE_STUDENT);
                    studentProfile = new Student();
                    studentProfile.setDateOfBirth(mDateOfBirth);
                    studentProfile.setName(mName);
                    studentProfile.setSurname(mSurname);
                    studentProfile.setIdNumber(mIdNumber);
                    studentProfile.setIsPublic(false);



                    ParseQuery<Degree> query = ParseQuery.getQuery(Degree.class);
                    query.whereEqualTo(Degree.TITLE, mDegreeTitle);
                    query.whereEqualTo(Degree.LEVEL, mDegreeLevel);

                    List<Degree> degreeList = query.find();

                    if(degreeList == null || degreeList.size() == 0){
                        Log.e(TAG, "Degree not found in database");
                        return false;
                    }

                    studentProfile.setDegree(degreeList.get(0));
                    studentProfile.setKeywords(studentProfile.createKeywordString());

                    studentProfile.save();

                    user.signUp();

                    studentProfile.setUser(user);
                    studentProfile.save();
                    user.setStudentProfile(studentProfile);
                    user.save();
                }

                else if(mIsTeacher){
                    user.setType(AppUser.PROFILE_TEACHER);
                    ParseQuery<Teacher> query = ParseQuery.getQuery(Teacher.class);

                    query.whereEqualTo(Teacher.ID_NUMBER, mIdNumber);

                    teacherProfile = query.getFirst();

                    if(teacherProfile == null)
                        return false;

                    teacherProfile.fetchIfNeeded();


                    if(teacherProfile.getUser() != null){
                        Log.e(TAG, "Teacher profile already linked to an app user.");
                        return false;
                    }

                    user.setName(teacherProfile.getName().trim());
                    user.setSurname(teacherProfile.getSurname().trim());
                    user.setPhone(teacherProfile.getPhone().trim());

                    user.signUp();
                    teacherProfile.setUser(user);

                    if(teacherProfile.containsKey(Teacher.COURSE_IDS_STRING) &&
                            teacherProfile.getString(Teacher.COURSE_IDS_STRING) != null){

                        ParseQuery<Course> teacherCoursesQuery = ParseQuery.getQuery(Course.class);
                        String[] teacherCourseIds = teacherProfile.getCourseIdsString().split("\\s+");

                        List<Course> courseList;
                        if(teacherCourseIds.length > 0){
                            teacherCoursesQuery.whereContainedIn(Course.CODE, Arrays.asList(teacherCourseIds));
                            teacherCoursesQuery.setLimit(100);

                            courseList = new ArrayList<>(teacherCoursesQuery.find());
                        }
                        else
                            courseList = new ArrayList<>();

                        if(courseList.size() == 0)
                            teacherProfile.setCourses(new ArrayList<Course>());
                        else
                            teacherProfile.setCourses(courseList);

                    }
                    teacherProfile.save();
                    user.setTeacherProfile(teacherProfile);
                    user.save();
                }
                else if(mIsCompany){
                    user.setType(AppUser.PROFILE_COMPANY);
                    companyProfile = new Company();
                    companyProfile.setVATNumber(mVatnumber);
                    companyProfile.setName(mName);
                    companyProfile.setIsPublic(false);

                    companyProfile.save();
                    user.signUp();
                    companyProfile.setUser(user);
                    companyProfile.save();
                    user.setCompanyProfile(companyProfile);
                    user.save();
                }

                return true;

            } catch (com.parse.ParseException e) {
                Log.e(TAG, "Unable to register the new user: " + mEmail, e);
                if(studentProfile != null)
                    studentProfile.deleteInBackground();
                if(teacherProfile != null)
                    teacherProfile.deleteInBackground();
                if(companyProfile != null)
                    companyProfile.deleteInBackground();

                user.deleteInBackground();

                return false;
            }
            catch (FileNotFoundException e) {
                Log.d(TAG, "Picture not found on device.", e);
                return false;
            }
            catch (IOException e) {
                Log.d(TAG, "An error occurred with input or output stream.", e);
                return false;
            }
            catch (InterruptedException e) {
                Log.d(TAG, "Save of picture interrupted.", e);
                return false;
            }
            finally {
                try {
                    if (outputStream != null)
                        outputStream.close();
                } catch (IOException e) {
                    Log.d(TAG, "Output stream already closed.");
                }
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                setResult(RESULT_OK);
                Log.d(TAG, "Signup completed with success.");
                finish();
            } else {
                setResult(RESULT_CANCELED);
                Toast.makeText(getApplicationContext(),getString(R.string.email_already_used),Toast.LENGTH_LONG).show();
                Log.d(TAG, "Signup canceled.");
                finish();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK && null != data) {
            try {
                if(pictureBitmap != null){
                    Log.d(TAG, "Recycling profile pic bitmap");
                    pictureBitmap.recycle();
                    pictureBitmap = null;
                }
                pictureUri = data.getData();
                if(pictureUri != null){
                    Log.d(TAG, "New picture Uri is : " + pictureUri.toString());
                    pictureBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), pictureUri);
                    pictureView.setColorFilter(Color.TRANSPARENT);
                    pictureView.setImageBitmap(pictureBitmap);
                    pictureView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    pictureView.invalidate();
                    Log.d(TAG, "Picture view invalidated.");
                    Snackbar.make(mEmailSignUpButton,R.string.undo_select_picture,Snackbar.LENGTH_LONG)
                            .setAction(R.string.undo, new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    pictureUri = null;
                                    pictureView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_picture));
                                    pictureView.setScaleType(ImageView.ScaleType.CENTER);
                                    pictureView.setColorFilter(getResources().getColor(R.color.colorAccent));
                                    pictureView.invalidate();

                                    pictureBitmap.recycle();
                                    pictureBitmap = null;
                                }
                            }).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


