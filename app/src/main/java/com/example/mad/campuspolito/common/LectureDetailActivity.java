package com.example.mad.campuspolito.common;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Location;
import com.example.mad.campuspolito.model.Teacher;
import com.example.mad.campuspolito.teacher.LectureEditorActivity;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

/**
 * @author kurado
 */
public class LectureDetailActivity extends AppCompatActivity {

    private static final String TAG = LectureDetailActivity.class.getName();
    public static final String LECTURE_ID = "Lecture_Id";
    public static final String LECTURE_POSITION = "Lecture_position";
    public static final int DETAIL_STATE = 0;
    public static final int LECTURE_STATUS_MODIFIED = 1;

    private Toolbar toolbar;
    private CardView content;
    private LinearLayout loadbar;
    private TextView course_name;
    private TextView professor;
    private TextView room;
    private TextView date;
    private TextView duration;
    private FloatingActionButton edit_fab;
    private MenuItem delete_button;
    private String lecture_id;
    private ArrayList<String> durationSlots;
    private FetchLectureDetails task;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecture_detail);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadbar = (LinearLayout)findViewById(R.id.load_progress);
        content = (CardView)findViewById(R.id.content);
        course_name = (TextView)findViewById(R.id.course_name);
        professor = (TextView)findViewById(R.id.teacher_name);
        room = (TextView)findViewById(R.id.lecture_room);
        date = (TextView)findViewById(R.id.lecture_date);
        duration = (TextView)findViewById(R.id.lecture_duration);
        edit_fab = (FloatingActionButton)findViewById(R.id.edit_lecture);
        delete_button = toolbar.getMenu().findItem(R.id.delete_lecture);

        lecture_id = getIntent().getStringExtra(LECTURE_ID);
        if(((AppUser)AppUser.getCurrentUser()).isTeacher() && lecture_id != null) {

            edit_fab.setVisibility(View.VISIBLE);
            edit_fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), LectureEditorActivity.class);
                    intent.putExtra(LECTURE_ID,lecture_id);
                    startActivityForResult(intent, LectureEditorActivity.EDIT_LECTURE);

                }
            });
        }
        durationSlots = new ArrayList<>();
        durationSlots.add("1:30");
        durationSlots.add("3:00");
        durationSlots.add("4:30");
        durationSlots.add("6:00");

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(lecture_id!=null) {
            if(task != null){
                task.cancel(false);
                task = null;
            }
            task = new FetchLectureDetails(this,lecture_id);
            task.execute();

        }else{
            Log.e(TAG, "Wrong parameters");
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null){
            task.cancel(false);
            task = null;
        }
    }

    private class DeleteLecture extends AsyncTask<Void,Void,Boolean> {
        private final Context ctx;
        private final String lectureId;

        public DeleteLecture(Context ctx, String lectureId){
            this.ctx = ctx;
            this.lectureId = lectureId;
        }

        @Override
        protected void onPreExecute() {
            content.setVisibility(View.GONE);
            loadbar.setVisibility(View.VISIBLE);
            edit_fab.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (lectureId != null) {
                try {
                    ParseQuery<Lecture> query = ParseQuery.getQuery(Lecture.class);
                    Lecture lecture = query.get(lectureId);
                    if(lecture == null)
                        return false;

                    lecture.getCourse().removeLecture(lecture);
                    lecture.getCourse().save();
                    lecture.delete();
                    Log.d(TAG,"Course saved and Lecture deleted successfully");
                    return true;
                } catch (ParseException e) {
                    Log.e(TAG, "Exception while deleting lecture", e);
                    return false;
                }
            }
            else return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){

                setResult(LECTURE_STATUS_MODIFIED);
                finish();
            }
            else{
                Snackbar.make(getCurrentFocus(),"Error while deleting lecture",Snackbar.LENGTH_LONG).show();
                content.setVisibility(View.VISIBLE);
                loadbar.setVisibility(View.VISIBLE);
                edit_fab.setVisibility(View.VISIBLE);
            }
        }
    }

    private class FetchLectureDetails extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final String lectureId;
        private StringBuilder stringBuilder;
        private Course course;
        private String date_info;
        private Lecture lecture;
        private Location location;

        public FetchLectureDetails(Context ctx, String lectureId){
            this.ctx = ctx;
            this.lectureId = lectureId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            content.setVisibility(View.GONE);
            loadbar.setVisibility(View.VISIBLE);
            edit_fab.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.d(TAG, "Retrieving lecture's details");

            ParseQuery<Lecture> query = ParseQuery.getQuery(Lecture.class);
            try {
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy, HH:mm",Locale.getDefault());

                lecture = query.get(lectureId);
                course = lecture.getCourse();
                course.fetchIfNeeded();


                ParseQuery<Teacher> teachersQuery = ParseQuery.getQuery(Teacher.class);
                teachersQuery.whereContains(Teacher.COURSE_IDS_STRING, course.getCode().toUpperCase());

                List<Teacher> teacher = new ArrayList<>(teachersQuery.find());


                stringBuilder = new StringBuilder();
                ListIterator<Teacher> li = teacher.listIterator();
                while(li.hasNext()){
                    Teacher t = li.next();
                    t.fetchIfNeeded();
                    stringBuilder.append(t.getName());
                    stringBuilder.append(" ");
                    stringBuilder.append(t.getSurname());
                    if(li.hasNext())
                        stringBuilder.append(", ");
                }


                location = lecture.getRoom();
                location.fetchIfNeeded();



                Calendar calendar = lecture.getStartCalendar();
                date_info = calendar.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.LONG,Locale.getDefault())+", "+dateFormat.format(calendar.getTime());

                return true;

            } catch (ParseException e) {
                Log.e(TAG, "Error while loading lecture's details", e);

                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                course_name.setText(course.getTitle());
                course_name.setPaintFlags(course_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                course_name.setTextColor(getResources().getColor(R.color.colorAccent));
                course_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), ShowCourseDetailsActivity.class);
                        intent.putExtra(ShowCourseDetailsActivity.COURSE_ID, course.getObjectId());
                        startActivity(intent);
                    }
                });
                professor.setText(stringBuilder.toString());
                room.setText(location.getLocation());

                date.setText(date_info);
                duration.setText(durationSlots.get(lecture.getDuration() - 1));

                content.setVisibility(View.VISIBLE);
                loadbar.setVisibility(View.GONE);
                if(((AppUser)AppUser.getCurrentUser()).isTeacher())
                    edit_fab.setVisibility(View.VISIBLE);

            }
            else {
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LectureEditorActivity.EDIT_LECTURE){
            if(resultCode == LectureEditorActivity.LECTURE_EDITED){
                Log.d(TAG,"Lecture modified");
                setResult(LECTURE_STATUS_MODIFIED);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lecture_detail, menu);
        if(!((AppUser)AppUser.getCurrentUser()).isTeacher()){
            menu.findItem(R.id.delete_lecture).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if(id == R.id.delete_lecture){
            if(((AppUser)AppUser.getCurrentUser()).isTeacher()){
                final Context ctx = this;
                new AlertDialog.Builder(this)
                        .setTitle("Delete lecture?")
                        .setMessage("This lecture will be removed from the timetable,are you sure?")
                        .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DeleteLecture task = new DeleteLecture(ctx,lecture_id);
                                task.execute();
                            }
                        }).create().show();
            }
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}
