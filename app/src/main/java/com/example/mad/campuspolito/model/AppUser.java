package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("_User")
public class AppUser extends ParseUser {

    public static final String PROFILE_COMPANY = "COMPANY";
    public static final String PROFILE_TEACHER = "TEACHER";
    public static final String PROFILE_STUDENT = "STUDENT";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String PICTURE = "picture";
    public static final String HIDDEN_CONVERSATIONS = "hidden_conversations";

    public AppUser(){

    }

    public void setName(String name){
        put(NAME, name);
    }
    public String getName(){
        return getString(NAME);
    }

    public void setSurname(String surname){
        put(SURNAME, surname);
    }
    public String getSurname(){
        return getString(SURNAME);
    }

    public void setType(String type){
        put(TYPE, type);
    }
    public String getType(){
        return getString(TYPE);
    }

    public boolean isStudent() {
        return getType().equals(PROFILE_STUDENT);
    }

    public void setStudentProfile(Student studentProfile){
        put(PROFILE_STUDENT, studentProfile);
    }
    public Student getStudentProfile(){
        return (Student)getParseObject(PROFILE_STUDENT);
    }

    public boolean isCompany() {
        return getType().equals(PROFILE_COMPANY);
    }

    public void setCompanyProfile(Company companyProfile){
        put(PROFILE_COMPANY, companyProfile);
    }
    public Company getCompanyProfile(){
        return (Company)getParseObject(PROFILE_COMPANY);
    }

    public boolean isTeacher() {
        return getType().equals(PROFILE_TEACHER);
    }
    public void setTeacherProfile(Teacher teacherProfile){
        put(PROFILE_TEACHER,teacherProfile);
    }
    public Teacher getTeacherProfile(){
        return (Teacher)getParseObject(PROFILE_TEACHER);
    }


    public String getPhone(){
        return getString(PHONE);
    }
    public void setPhone(String phone){
        put(PHONE, phone);
    }

    public String getEmail(){
        return getString(EMAIL);
    }
    public void setEmail(String email){
        put(EMAIL, email);
    }


    public ParseFile getPicture(){
        return getParseFile(PICTURE);
    }
    public void setPicture(ParseFile picture){
        put(PICTURE, picture);
    }


    @Override
    public String toString() {
        if(isStudent() || isTeacher())
            return getName() + " " + getSurname();
        else
            return getName();
    }

    public void setHiddenConversations(List<String> hiddenConversations){
        put(HIDDEN_CONVERSATIONS, hiddenConversations);
    }
    public List<String> getHiddenConversations(){
        return getList(HIDDEN_CONVERSATIONS);
    }
    public void addHiddenConversation(String conversation){
        add(HIDDEN_CONVERSATIONS, conversation);
    }
    public void removeHiddenConversation(String conversation){
        removeAll(HIDDEN_CONVERSATIONS, Arrays.asList(conversation));
    }
}
