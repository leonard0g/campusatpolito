package com.example.mad.campuspolito.locations;

/**
 * Created by lg on 04/08/15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.SyncStateContract;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.common.FetchAddressIntentService;
import com.example.mad.campuspolito.model.Location;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.mad.campuspolito.R;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link LocationSearchMasterActivity}
 * in two-pane mode (on tablets) or a {@link LocationSearchDetailActivity}
 * on handsets.
 */
public class LocationSearchDetailFragment extends Fragment
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, ISetBounds {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    //public static final String SITE = "site";
    //public static final String TYPE = "type";
    public static final String IDS = "ids";

    private static final String TAG = LocationSearchDetailFragment.class.getName();
    private static final float ZOOM = 18F;
    private static final int PADDING = 100;

    private boolean mResolvingError = false;
    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private static final String DIALOG_ERROR = "dialog_error"; //TODO

    private String site;
    private String type;
    private ArrayList<String> ids;
    private Map<Marker, Location> markersMap;
    private LatLngBounds.Builder bounds;
    private GoogleMap googleMap;

    private TextView clickMarkerForDetailsTV;
    private TextView siteTV;
    private TextView floorTV;
    private TextView detailsTV;
    private TextView addressTV;
    private LinearLayout detailsContainerLL;
    private LinearLayout mProgress;
    private LinearLayout mContent;
    private String selectedId;
    private static final String SELECTED_ID = "selected_id";
    private Toolbar toolbar;

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Show a toast message if an address was found.
            if (resultCode == FetchAddressIntentService.SUCCESS_RESULT) {
                String addressOutput = resultData.getString(FetchAddressIntentService.RESULT_DATA_KEY);
                addressTV.setText(addressOutput);
            }

        }
    }

    protected android.location.Location mLastLocation;
    private AddressResultReceiver mResultReceiver;

    protected void startIntentService() {
        Intent intent = new Intent(getActivity(), FetchAddressIntentService.class);
        intent.putExtra(FetchAddressIntentService.RECEIVER, mResultReceiver);
        intent.putExtra(FetchAddressIntentService.LOCATION_DATA_EXTRA, mLastLocation);
        getActivity().startService(intent);
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LocationSearchDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        selectedId = null;
        mResultReceiver = new AddressResultReceiver(new Handler());
        if (savedInstanceState == null) {
            if (getArguments().containsKey(LocationSearchDetailFragment.IDS)) {
                Log.i(TAG, "onCreate, savedInstanceState is null");
                //site = getArguments().getString(SITE);
                //type = getArguments().getString(TYPE);
                ids = getArguments().getStringArrayList(IDS);

                if(getArguments().containsKey(SELECTED_ID))
                    selectedId = getArguments().getString(SELECTED_ID);

                Log.i(TAG, site + " " + type + " #" + ids.size());
            }
        } else {
            Log.i(TAG, "onCreate, restoring instance state");
            //site = savedInstanceState.getString(SITE);
            //type = savedInstanceState.getString(TYPE);
            ids = savedInstanceState.getStringArrayList(IDS);
            selectedId = savedInstanceState.getString(SELECTED_ID);
        }

        buildGoogleApiClient();
        CampusPolitoApplication.getGoogleApiClient().connect();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(getActivity() instanceof LocationSearchDetailActivity){
            toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        }
        else
            toolbar = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_location_search_detail, container, false);

        if(getActivity() != null){
            if(getActivity() instanceof LocationSearchDetailActivity){
                toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
            }
        }
        else
            toolbar = null;

        detailsContainerLL = (LinearLayout) rootView.findViewById(R.id.ll);
        siteTV = (TextView) rootView.findViewById(R.id.site);
        floorTV = (TextView) rootView.findViewById(R.id.floor);
        detailsTV = (TextView) rootView.findViewById(R.id.details);
        addressTV = (TextView)rootView.findViewById(R.id.address);
        clickMarkerForDetailsTV = (TextView) rootView.findViewById(R.id.click_marker);

        mContent = (LinearLayout)rootView.findViewById(R.id.map_container);
        mProgress = (LinearLayout)rootView.findViewById(R.id.details_progress);

        if (ids.size() > 1) {
            clickMarkerForDetailsTV.setVisibility(View.VISIBLE);
        } else {
            detailsContainerLL.setVisibility(View.VISIBLE);
            getLocation(ids.get(0), new GetCallback<Location>() {
                @Override
                public void done(Location location, ParseException e) {
                    if (e == null) {
                        siteTV.setText(location.getSite());
                        floorTV.setText(location.getFloor());
                        detailsTV.setText(location.getDetails());
                        if(toolbar != null)
                            toolbar.setTitle(location.getLocation());

                        mLastLocation = new android.location.Location("LocationProvider.");
                        mLastLocation.setLatitude(location.getLat());
                        mLastLocation.setLongitude(location.getLng());
                        if(CampusPolitoApplication.getGoogleApiClient().isConnected())
                            startIntentService();

                    } else {
                        Log.i(TAG, "getLocation error: " + e.getMessage());
                    }
                }
            });
        }

        MapFragment mMapFragment = MapFragment.newInstance();
        mMapFragment.getMapAsync(this);
        getActivity().getFragmentManager().beginTransaction()
                .replace(R.id.map_container, mMapFragment)
                .commit();

        mContent.setVisibility(View.INVISIBLE);
        mProgress.setVisibility(View.VISIBLE);

        return rootView;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putString(LocationSearchDetailFragment.SITE, site);
        //outState.putString(LocationSearchDetailFragment.TYPE, type);
        outState.putStringArrayList(LocationSearchDetailFragment.IDS, ids);
        if(selectedId != null)
            outState.putString(SELECTED_ID, selectedId);
    }


    @Override
    public void onMapReady(final GoogleMap map) {
        Log.i(TAG, "onMapReady");
        googleMap = map;
        googleMap.setMyLocationEnabled(true);

        //googleMap.setInfoWindowAdapter(new CustomWindowAdapter(getLayoutInflater()));
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                if (markersMap.size() > 1) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), ZOOM));
                    clickMarkerForDetailsTV.setVisibility(View.GONE);
                    detailsContainerLL.setVisibility(View.VISIBLE);
                    Location location = markersMap.get(marker);
                    siteTV.setText(location.getSite());
                    floorTV.setText(location.getFloor());
                    detailsTV.setText(location.getDetails());
                    selectedId = marker.getId();
                    if (toolbar != null)
                        toolbar.setTitle(location.getLocation());
                    mLastLocation = new android.location.Location("LocationProvider.");
                    mLastLocation.setLatitude(location.getLat());
                    mLastLocation.setLongitude(location.getLng());
                    if(CampusPolitoApplication.getGoogleApiClient().isConnected())
                        startIntentService();
                    return true;
                }
                return false;
            }
        });

        MyTask task = new MyTask(this);
        task.execute(ids);

    }


    //TODO provare primo param void, usare ids
    private class MyTask extends AsyncTask<List<String>, Void,Boolean> {

        private ISetBounds listener;

        public MyTask(ISetBounds listener) {
            this.listener = listener;
        }

        @SafeVarargs
        @Override
        protected final Boolean doInBackground(List<String>... params) {

            final int[] count = new int[1];
            boolean done = false;
            final List<String> _ids = params[0];
            bounds = new LatLngBounds.Builder();
            markersMap = new HashMap<Marker, Location>();

            fetchLocations(_ids, new FindCallback<Location>() {
                @Override
                public void done(List<Location> list, ParseException e) {
                    if (e != null)
                        Log.e(TAG, "Error while fetching locations from _ids", e);
                    else {

                        for (Location l : list) {
                            Marker marker = googleMap.addMarker(new MarkerOptions()
                                    .title(l.getLocation())
                                    .snippet(l.getFloor())
                                    .position(new LatLng(l.getLat(), l.getLng()))
                                    .icon(BitmapDescriptorFactory.defaultMarker(/*BitmapDescriptorFactory.HUE_BLUE*/)));
                            markersMap.put(marker, l);

                            bounds.include(marker.getPosition());

                        }

                        listener.boundsDone(bounds);
                    }
                }
            });

            return true;
        }
    }

    @Override
    public void boundsDone(LatLngBounds.Builder bounds) {
        mContent.setVisibility(View.VISIBLE);
        mProgress.setVisibility(View.GONE);
        Log.i(TAG, "done, setting bounds");
        if (markersMap.size() == 1) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bounds.build().getCenter(), ZOOM));
            ((Marker)markersMap.keySet().toArray()[0]).showInfoWindow();
        } else {

            if(selectedId != null) {
                for(Marker m : markersMap.keySet()){
                    if(m.getId().equals(selectedId)){
                        Location l = markersMap.get(m);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(m.getPosition(), ZOOM));
                        siteTV.setText(l.getSite());
                        floorTV.setText(l.getFloor());
                        detailsTV.setText(l.getDetails());
                        clickMarkerForDetailsTV.setVisibility(View.GONE);
                        detailsContainerLL.setVisibility(View.VISIBLE);
                        m.showInfoWindow();

                        if(toolbar != null)
                            toolbar.setTitle(l.getLocation());
                        mLastLocation = new android.location.Location("LocationProvider.");
                        mLastLocation.setLatitude(l.getLat());
                        mLastLocation.setLongitude(l.getLng());
                        if(CampusPolitoApplication.getGoogleApiClient().isConnected())
                            startIntentService();

                        return;
                    }
                }
            }
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(),PADDING));

        }
    }


    public void getLocation(String objectId, GetCallback<Location> callback) {
        ParseQuery<Location> query = ParseQuery.getQuery(Location.class);
        query.getInBackground(objectId, callback);
    }

    public void fetchLocations(List<String> objectIds, FindCallback<Location> callback){
        ParseQuery<Location> query = ParseQuery.getQuery(Location.class);
        query.whereContainedIn("objectId", objectIds);

        query.findInBackground(callback);
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "buildGoogleApiClient");
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        CampusPolitoApplication.setGoogleApiClient(mGoogleApiClient);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
        CampusPolitoApplication.getGoogleApiClient().disconnect();
        super.onStop();
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart");
        super.onStart();
        if (!mResolvingError) {  // more about this later
            CampusPolitoApplication.getGoogleApiClient().connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "onConnectionFailed " + result.toString());
        if (!mResolvingError) {
            // We are not attempting to resolve an error.
            if (result.hasResolution()) {
                try {
                    mResolvingError = true;
                    result.startResolutionForResult(getActivity(), REQUEST_RESOLVE_ERROR);
                } catch (IntentSender.SendIntentException e) {
                    // There was an error with the resolution intent. Try again.
                    CampusPolitoApplication.getGoogleApiClient().connect();
                }
            }
            else {
                // Show dialog using GooglePlayServicesUtil.getErrorDialog()
                showErrorDialog(result.getErrorCode());
                mResolvingError = true;
            }
        }
    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "some error");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    /* A fragment to display an error dialog */
    public class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            onDialogDismissed();
        }
    }

}
