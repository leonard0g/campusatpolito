**Mobile Application Development course project, ay 2014-2015**
# Campus@Polito

This Android application supports students in their day-to-day campus life, providing help in finding didactical information, social interactions, and temporary-job placement.

Using the application, a student can locate classrooms; find out lecture timetables and teachers' consulting hours; receive notifications about changes in their daily schedule.

Students can also interact with each other exchanging messages (one-to-one or one-to-many, supporting different target groups), and access a shared noticeboard where categorized notices can be published (e.g.: looking for rent, selling used books and notes, ...).

Each student can also maintain a profile, where her/his competences are listed and can be accessed by companies offering temporary jobs opportunities.